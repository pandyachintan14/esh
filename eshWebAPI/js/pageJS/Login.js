﻿
function hideloading() {
    var myVar = setTimeout($("#loader").css('display', 'none'), 1000);
}

function showloading() {
    var myVar = setTimeout($("#loader").css('display', 'block'), 1000);
}

$("#btnlogin").click(function () {
    debugger;
    showloading();
    var username = $('#txtusername').val();
    var password = $('#txtPassword').val();
    if (username == '' || username == null) {
        $('#lblerror1').css('display', 'block');
        $('#lblerror1').text("Enter UserName");
    }
    else {
        $('#lblerror1').css('display', 'none');
    }
    if (password == '' || password == null) {
        $('#lblerror2').css('display', 'block');
        $('#lblerror2').text("Enter Password");
    }
    else {
        $('#lblerror2').css('display', 'none');
    }

    if ((username != "" && username != null) && (password != "" && password != null)) {
        Login();
    }

});


function Login() {
    debugger;
    showloading();
    var username = $('#txtusername').val();
    var password = $('#txtPassword').val();
    var UserPK = -1;

    var user = JSON.stringify({ "Flag": "Check", "UserName": username, "Password": password, "UserPK": UserPK });

 


    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/Home/checkLogin",
        data: user,
        async: false,
        dataType: "json",
        success: OnSuccess,
        error: function (XMLHttpRequest) {

            if (XMLHttpRequest.status == 401) {
                // alert(SessionClientID);
                window.location.href = '/Login/Signin';
            }
        },
    });
    function OnSuccess(data) {
        showloading();
        debugger;
        if (data != -1) {

            if (data.payload == null) {
                $('#lblerror').css('display', 'block');
                $('#lblerror').text("Invalid Username Or Password");
                return false;
            }
            if (data.payload != null) {
                if (data.payload.LoginStatus == 1)  {
                    debugger;
                    $("#hdnLoginPK").val(data.payload.UserPK);
                    window.location.href = '/Home/Dashboard';
                }


                if (data.payload.LoginStatus == 2) {
                    $('#lblerror').css('display', 'block');
                    $('#lblerror').text("Invalid Username Or Password");
                    hideloading();
                    return false;
                }
                 
            }
        }
        else {
            $('#lblerror').css('display', 'block');
            $('#lblerror').text("Invalid Username Or Password");
            hideloading();
            return false;
        }
    }

}