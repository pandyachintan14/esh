﻿using eSH.Model.Api.Request;
using eSH.Model.Api.Response;
using eSH.Service.ApiServices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;

namespace eshWebAPI.Controllers
{
    public class HomeController : Controller
    {

        
        public ActionResult Dashboard()
        {
            ViewBag.SessionUsername = HttpContext.Session["SessionUsername"];
            ViewBag.SessionEmailAddress = HttpContext.Session["SessionEmailAddress"];
            ViewBag.SessionContactNo = HttpContext.Session["SessionContactNo"];
            ViewBag.UserPK = HttpContext.Session["UserPK"];
            ViewBag.Member_PK = HttpContext.Session["Member_PK"];
            ViewBag.Registration_Number = HttpContext.Session["Registration_Number"];
            ViewBag.Member_Full_Name = HttpContext.Session["Member_Full_Name"];
            ViewBag.Membership_Type = HttpContext.Session["Membership_Type"];
            return View();
        }
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult MyProfile()
        {
            ViewBag.SessionUsername = HttpContext.Session["SessionUsername"];
            ViewBag.SessionEmailAddress = HttpContext.Session["SessionEmailAddress"];
            ViewBag.SessionContactNo = HttpContext.Session["SessionContactNo"];
            ViewBag.UserPK = HttpContext.Session["UserPK"];
            ViewBag.Member_PK = HttpContext.Session["Member_PK"];
            ViewBag.Registration_Number = HttpContext.Session["Registration_Number"];
            ViewBag.Member_Full_Name = HttpContext.Session["Member_Full_Name"];
            ViewBag.Membership_Type = HttpContext.Session["Membership_Type"];
            return View();
        }
        public ActionResult Tree()
        {
            ViewBag.SessionUsername = HttpContext.Session["SessionUsername"];
            ViewBag.SessionEmailAddress = HttpContext.Session["SessionEmailAddress"];
            ViewBag.SessionContactNo = HttpContext.Session["SessionContactNo"];
            ViewBag.UserPK = HttpContext.Session["UserPK"];
            ViewBag.Member_PK = HttpContext.Session["Member_PK"];
            ViewBag.Registration_Number = HttpContext.Session["Registration_Number"];
            ViewBag.Member_Full_Name = HttpContext.Session["Member_Full_Name"];
            ViewBag.Membership_Type = HttpContext.Session["Membership_Type"];
            return View();
        }
        public ActionResult AddNewMember()
        {
            ViewBag.SessionUsername = HttpContext.Session["SessionUsername"];
            ViewBag.SessionEmailAddress = HttpContext.Session["SessionEmailAddress"];
            ViewBag.SessionContactNo = HttpContext.Session["SessionContactNo"];
            ViewBag.UserPK = HttpContext.Session["UserPK"];
            ViewBag.Member_PK = HttpContext.Session["Member_PK"];
            ViewBag.Registration_Number = HttpContext.Session["Registration_Number"];
            ViewBag.Member_Full_Name = HttpContext.Session["Member_Full_Name"];
            ViewBag.Membership_Type = HttpContext.Session["Membership_Type"];
            return View();
        }

        [HttpPost]
       
        [Route("checkLogin")]
        public JsonResult checkLogin( User data)
        {
            Login euvm = new Login();
            try
            {
                LoginService _loginServices = new LoginService();
                euvm = _loginServices.checkLogin(data);

                HttpContext.Session["SessionUsername"]= euvm.payload.SessionUsername;
                HttpContext.Session["SessionEmailAddress"]= euvm.payload.SessionEmailAddress;
                HttpContext.Session["SessionContactNo"] = euvm.payload.SessionContactNo;
                HttpContext.Session["UserPK"] = euvm.payload.UserPK.ToString();
                HttpContext.Session["Member_PK"]= euvm.payload.Member_PK;
                HttpContext.Session["Registration_Number"]= euvm.payload.Registration_Number;
                HttpContext.Session["Member_Full_Name"]= euvm.payload.Member_Full_Name;
                HttpContext.Session["Membership_Type"]= euvm.payload.Membership_Type;
            }
            catch (Exception ex)
            {
            }
            return Json(euvm);

        }
    }
}