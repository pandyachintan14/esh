﻿using eSH.Data.Repositories;
using eSH.Model.Api.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace eshWebAPI.Controllers
{
 
    public class UtilitiesAPIController : ApiController
    {
        // POST api/values
     
        [HttpPost]
        [Route("api/Utilities/GetGeneologyTree")]
        public BaseResponseModel GetGeneologyTree( BaseRequestModel brem)
        {
            BaseResponseModel brm = new BaseResponseModel();
            GeneologyTreeRepository geneologyTreeRepository = new GeneologyTreeRepository();
            try
            {
                brm = geneologyTreeRepository.GetGeneologyTree(brem);
            }
            catch (Exception ex)
            {
            }
            return brm;

        }


    }
}