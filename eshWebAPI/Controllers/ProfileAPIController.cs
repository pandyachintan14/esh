﻿using eSH.Data.Repositories;
using eSH.Model.Api.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace eshWebAPI.Controllers
{
 
    public class ProfileAPIController : ApiController
    {
        // POST api/values
        [HttpPost]
        [Route("api/Profile/GetMemberProfileDetails")]
        public BaseResponseModel GetMemberProfileDetails( BaseRequestModel brem)
        {
            //ManageMemberProfileRepository

            BaseResponseModel brm = new BaseResponseModel();
            ManageMemberProfileRepository manageMemberProfileRepository = new ManageMemberProfileRepository();
            try
            {
                brm = manageMemberProfileRepository.GetMemberProfileDetails(brem);
            }
            catch (Exception ex)
            {
            }
            return brm;
        }
    }
}