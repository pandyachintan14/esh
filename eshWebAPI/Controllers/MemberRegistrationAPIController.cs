﻿using eSH.Data.Repositories;
using eSH.Model.Api.Common;
using eSH.Model.Api.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace eshWebAPI.Controllers
{
 
    public class MemberRegistrationAPIController : ApiController
    { // POST api/values
        [HttpPost]
        [Route("api/MemberRegistration/ManageMemberRegistrations")]
        public BaseResponseModel ManageMemberRegistrations([FromBody]RegistrationRequestInformation rri)
        {
            BaseResponseModel brm = new BaseResponseModel();
            RegistrationRepository registrationRepository = new RegistrationRepository();
            try
            {
                brm = registrationRepository.ManageMemberRegistrations(rri);
            }
            catch (Exception ex)
            {
            }
            return brm;
        }
        [HttpPost]
        [Route("api/MemberRegistration/GetMemberDetailsSponserWise")]
        public BaseResponseModel GetMemberDetailsSponserWise([FromBody]BaseRequestModel brem)
        {
            BaseResponseModel brm = new BaseResponseModel();
            ManageMemberDetailsRepository manageMemberDetailsRepository = new ManageMemberDetailsRepository();
            try
            {
                brm = manageMemberDetailsRepository.GetMemberDetailsSponserWise(brem);
            }
            catch (Exception ex)
            {
            }
            return brm;
        }
        [HttpPost]
        [Route("api/MemberRegistration/GetSponsersList")]
        public BaseResponseModel GetSponsersList( BaseRequestModel brem)
        {
            BaseResponseModel brm = new BaseResponseModel();
            ManageSponsersRepository manageSponsersRepository = new ManageSponsersRepository();
            try
            {
                brm = manageSponsersRepository.GetSponsersList(brem);
            }
            catch (Exception ex)
            {
            }
            return brm;
        }
    }
}