﻿ 
using eSH.Model.Api.Common;
using eSH.Model.Api.Response;
using eshWebAPI.Models.eSH.Data.CommonUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace eSH.Data.Repositories
{
   public class ManageSponsersRepository
    {
        CommonUtility cu = new  CommonUtility();

        public BaseResponseModel GetSponsersList(BaseRequestModel brem)
        {
            MasterSponsorsResponseModel msrm = new MasterSponsorsResponseModel();
            List<MasterSponsorsResponseModel> lmsrm = new List<MasterSponsorsResponseModel>();
            BaseResponseModel brm = new BaseResponseModel();

            try
            {
                SqlParameter[] param = new SqlParameter[1];

                param[0] = new SqlParameter("@LoginID", brem.LoginID);

                DataSet ds = DataBaseAccessLayer.Select(CommonSP.usp_GetSponserDetails, param);

                if (cu.checkDatasetRecords(ds))
                {
                    if (cu.checkDataTableRecords(ds.Tables[0]))
                    {
                        if (ds.Tables[0].Rows[0][0].ToString() == "1")
                        {
                            if (ds.Tables.Count > 1 && cu.checkDataTableRecords(ds.Tables[1]))
                            {
                                //in case of GET records it will come.
                                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                                {
                                    msrm = new MasterSponsorsResponseModel();
                                    msrm.Sponser_PK = Convert.ToInt32(ds.Tables[1].Rows[i]["Sponser_PK"]);
                                    msrm.Sponser_Name= Convert.ToString(ds.Tables[1].Rows[i]["Sponser_Name"]);
                                    msrm.Sponser_RegCode = Convert.ToString(ds.Tables[1].Rows[i]["Sponser_RegCode"]);
                                    msrm.Sponser_Status = Convert.ToString(ds.Tables[1].Rows[i]["Sponser_Status"]);
                                    lmsrm.Add(msrm);
                                }
                                brm.Status = 1;
                                brm.Exception = null;
                                brm.Message = "Success";
                                brm.Payload = lmsrm;
                                brm.ValidationSummary = "";
                            }
                            else
                            {
                                brm.Status = 1;
                                brm.Exception = null;
                                brm.Message = "Success";
                                brm.Payload = null;
                                brm.ValidationSummary = Convert.ToString(ds.Tables[0].Rows[0][1]);
                            }
                        }
                        else
                        {
                            brm.Status = 0;
                            brm.Exception = null;
                            brm.Message = "Fail";
                            brm.Payload = null;
                            brm.ValidationSummary = Convert.ToString(ds.Tables[0].Rows[0][1]);
                        }
                    }
                    else
                    {
                        brm.Status = 0;
                        brm.Exception = "No Records Found.";
                        brm.Message = "Technical Error";
                        brm.Payload = null;
                        brm.ValidationSummary = "No Records Found.";
                    }
                }
                else
                {
                    brm.Status = 0;
                    brm.Exception = "No Records Found.";
                    brm.Message = "Technical Error";
                    brm.Payload = null;
                    brm.ValidationSummary = "No Records Found.";
                }

            }
            catch (Exception ex)
            {
                brm.Status = 0;
                brm.Exception = ex.Message.ToString();
                brm.Message = "Technical Error";
                brm.Payload = null;
                brm.ValidationSummary = null;
            }
            return brm;
        }
    }
}
