﻿using eSH.Model.Api.Common;
using eSH.Model.Api.Response;
using eshWebAPI.Models.eSH.Data.CommonUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace eSH.Data.Repositories
{
  public class GeneologyTreeRepository
    {

        CommonUtility cu = new CommonUtility();

        public BaseResponseModel GetGeneologyTree(BaseRequestModel brem)
        {
            GeneologyTreeResponseModel gtrm = new GeneologyTreeResponseModel();
            List<GeneologyTreeResponseModel> lgtrm = new List<GeneologyTreeResponseModel>();
            BaseResponseModel brm = new BaseResponseModel();

            try
            {
                SqlParameter[] param = new SqlParameter[1];

                param[0] = new SqlParameter("@LoginID", brem.LoginID);

                DataSet ds = DataBaseAccessLayer.Select(CommonSP.usp_GetGeneologyTreeDetails, param);

                if (cu.checkDatasetRecords(ds))
                {
                    if (cu.checkDataTableRecords(ds.Tables[0]))
                    {
                        if (ds.Tables[0].Rows[0][0].ToString() == "1")
                        {
                            if (ds.Tables.Count > 1 && cu.checkDataTableRecords(ds.Tables[1]))
                            {
                                //in case of GET records it will come.
                                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                                {
                                    gtrm = new GeneologyTreeResponseModel();


                                    gtrm.id = Convert.ToInt32(ds.Tables[1].Rows[i]["id"]);
                                    gtrm.pid = Convert.ToInt32(ds.Tables[1].Rows[i]["pid"]==null?0: ds.Tables[1].Rows[i]["pid"]);
                                    gtrm.name = Convert.ToString(ds.Tables[1].Rows[i]["name"]);
                                    gtrm.title = Convert.ToString(ds.Tables[1].Rows[i]["title"]);
                                    gtrm.email = Convert.ToString(ds.Tables[1].Rows[i]["email"]);
                                    gtrm.img = Convert.ToString(ds.Tables[1].Rows[i]["img"]);


                                    lgtrm.Add(gtrm);
                                }
                                brm.Status = 1;
                                brm.Exception = null;
                                brm.Message = "Success";
                                brm.Payload = lgtrm;
                                brm.ValidationSummary = "";
                            }
                            else
                            {
                                brm.Status = 1;
                                brm.Exception = null;
                                brm.Message = "Success";
                                brm.Payload = null;
                                brm.ValidationSummary = Convert.ToString(ds.Tables[0].Rows[0][1]);
                            }
                        }
                        else
                        {
                            brm.Status = 0;
                            brm.Exception = null;
                            brm.Message = "Fail";
                            brm.Payload = null;
                            brm.ValidationSummary = Convert.ToString(ds.Tables[0].Rows[0][1]);
                        }
                    }
                    else
                    {
                        brm.Status = 0;
                        brm.Exception = "No Records Found.";
                        brm.Message = "Technical Error";
                        brm.Payload = null;
                        brm.ValidationSummary = "No Records Found.";
                    }
                }
                else
                {
                    brm.Status = 0;
                    brm.Exception = "No Records Found.";
                    brm.Message = "Technical Error";
                    brm.Payload = null;
                    brm.ValidationSummary = "No Records Found.";
                }

            }
            catch (Exception ex)
            {
                brm.Status = 0;
                brm.Exception = ex.Message.ToString();
                brm.Message = "Technical Error";
                brm.Payload = null;
                brm.ValidationSummary = null;
            }
            return brm;
        }
    }
}
