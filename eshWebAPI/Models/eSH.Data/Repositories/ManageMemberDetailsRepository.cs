﻿using eSH.Model.Api.Common;
using eSH.Model.Api.Response;
using eshWebAPI.Models.eSH.Data.CommonUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace eSH.Data.Repositories
{
    public class ManageMemberDetailsRepository
    {
        CommonUtility cu = new CommonUtility();

        public BaseResponseModel GetMemberDetailsSponserWise(BaseRequestModel brem)
        {
            MemberDetailsResponseModel mdrm = new MemberDetailsResponseModel();
            MemberDetailsSubResponseModel mdsrm = new MemberDetailsSubResponseModel();
            List<MemberDetailsResponseModel> lmdrm = new List<MemberDetailsResponseModel>();
            List < MemberDetailsSubResponseModel> lmdsrm = new List<MemberDetailsSubResponseModel>();
            BaseResponseModel brm = new BaseResponseModel();

            try
            {
                SqlParameter[] param = new SqlParameter[2];

                param[0] = new SqlParameter("@LoginID", brem.LoginID);
                param[1] = new SqlParameter("@Flag", brem.Flag);

                DataSet ds = DataBaseAccessLayer.Select(CommonSP.usp_ManagesMemberApproval, param);

                if (cu.checkDatasetRecords(ds))
                {
                    if (cu.checkDataTableRecords(ds.Tables[0]))
                    {
                        if (ds.Tables[0].Rows[0][0].ToString() == "1")
                        {
                            if (ds.Tables.Count > 1 && cu.checkDataTableRecords(ds.Tables[1]))
                            {
                                //in case of GET records it will come.
                                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                                {
                                    mdrm = new MemberDetailsResponseModel();
                                    mdrm.Parent_Name = Convert.ToString(ds.Tables[1].Rows[i]["Parent_Name"]);
                                    mdrm.Parent_FK = Convert.ToInt32(ds.Tables[1].Rows[i]["Parent_FK"]);
                                    lmdsrm = new List<MemberDetailsSubResponseModel>();
                                    for (int j = 0; j < ds.Tables[2].Rows.Count; j++)
                                    {
                                        if (Convert.ToInt32(ds.Tables[1].Rows[i]["Parent_FK"]) == Convert.ToInt32(ds.Tables[2].Rows[j]["Parent_FK"]))
                                        {
                                            mdsrm = new MemberDetailsSubResponseModel();
                                            mdsrm.Child_Pk= Convert.ToInt32(ds.Tables[2].Rows[j]["Child_Pk"]);
                                            mdsrm.Child_Name= Convert.ToString(ds.Tables[2].Rows[j]["Child_Name"]);
                                            mdsrm.Child_Rejection_Reason= Convert.ToString(ds.Tables[2].Rows[j]["Child_Rejection_Reason"]);
                                            mdsrm.isApproved= Convert.ToInt32(ds.Tables[2].Rows[j]["isApproved"]);
                                            mdsrm.isTC= Convert.ToInt32(ds.Tables[2].Rows[j]["isTC"]);
                                            lmdsrm.Add(mdsrm);
                                        }

                                    }
                                    mdrm.memberChild = lmdsrm;

                                    lmdrm.Add(mdrm);
                                }
                                brm.Status = 1;
                                brm.Exception = null;
                                brm.Message = "Success";
                                brm.Payload = lmdrm;
                                brm.ValidationSummary = "";
                            }
                            else
                            {
                                brm.Status = 1;
                                brm.Exception = null;
                                brm.Message = "Success";
                                brm.Payload = null;
                                brm.ValidationSummary = Convert.ToString(ds.Tables[0].Rows[0][1]);
                            }
                        }
                        else
                        {
                            brm.Status = 0;
                            brm.Exception = null;
                            brm.Message = "Fail";
                            brm.Payload = null;
                            brm.ValidationSummary = Convert.ToString(ds.Tables[0].Rows[0][1]);
                        }
                    }
                    else
                    {
                        brm.Status = 0;
                        brm.Exception = "No Records Found.";
                        brm.Message = "Technical Error";
                        brm.Payload = null;
                        brm.ValidationSummary = "No Records Found.";
                    }
                }
                else
                {
                    brm.Status = 0;
                    brm.Exception = "No Records Found.";
                    brm.Message = "Technical Error";
                    brm.Payload = null;
                    brm.ValidationSummary = "No Records Found.";
                }

            }
            catch (Exception ex)
            {
                brm.Status = 0;
                brm.Exception = ex.Message.ToString();
                brm.Message = "Technical Error";
                brm.Payload = null;
                brm.ValidationSummary = null;
            }
            return brm;
        }
       
    }
}
