﻿ 
using eSH.Model.Api.Common;
using eSH.Model.Api.Response;
using eshWebAPI.Models.eSH.Data.CommonUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace eSH.Data.Repositories
{
  public class ManageMemberProfileRepository
    {
        CommonUtility cu = new CommonUtility();

        public BaseResponseModel GetMemberProfileDetails(BaseRequestModel brem)
        {
            ProfileManageResponseModel pmrm = new ProfileManageResponseModel();
            List<ProfileManageResponseModel> lpmrm = new List<ProfileManageResponseModel>();
            BaseResponseModel brm = new BaseResponseModel();

            try
            {
                SqlParameter[] param = new SqlParameter[1];

                param[0] = new SqlParameter("@LoginID", brem.LoginID);

                DataSet ds = DataBaseAccessLayer.Select(CommonSP.usp_GetMemberProfileDetails, param);

                if (cu.checkDatasetRecords(ds))
                {
                    if (cu.checkDataTableRecords(ds.Tables[0]))
                    {
                        if (ds.Tables[0].Rows[0][0].ToString() == "1")
                        {
                            if (ds.Tables.Count > 1 && cu.checkDataTableRecords(ds.Tables[1]))
                            {
                                //in case of GET records it will come.
                                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                                {
                                    pmrm = new ProfileManageResponseModel();
                                    pmrm.Registration_Number = Convert.ToString(ds.Tables[1].Rows[i]["Registration_Number"]);
                                    pmrm.Member_Pk = Convert.ToInt32(ds.Tables[1].Rows[i]["Member_Pk"]);
                                    pmrm.Member_Photo_URL = Convert.ToString(ds.Tables[1].Rows[i]["Member_Photo_URL"]);
                                    pmrm.Member_FullName = Convert.ToString(ds.Tables[1].Rows[i]["Member_FullName"]);
                                    pmrm.Member_Section_1 = Convert.ToString(ds.Tables[1].Rows[i]["Member_Section_1"]);
                                    pmrm.Member_Section_1_Value = Convert.ToString(ds.Tables[1].Rows[i]["Member_Section_1_Value"]);
                                    pmrm.Member_Section_2 = Convert.ToString(ds.Tables[1].Rows[i]["Member_Section_2"]);
                                    pmrm.Member_Section_2_Value = Convert.ToString(ds.Tables[1].Rows[i]["Member_Section_2_Value"]);
                                    pmrm.Member_Section_3 = Convert.ToString(ds.Tables[1].Rows[i]["Member_Section_3"]);
                                    pmrm.Member_Section_3_Value = Convert.ToString(ds.Tables[1].Rows[i]["Member_Section_3_Value"]);
                                    pmrm.Member_Personal_Full_Name = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_Full_Name"]);
                                    pmrm.Member_Personal_Citizenship = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_Citizenship"]);
                                    pmrm.Member_Personal_DateOfBirth = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_DateOfBirth"]);
                                    pmrm.Member_Personal_Gender = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_Gender"]);
                                    pmrm.Member_Personal_Ethinicity = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_Ethinicity"]);
                                    pmrm.Member_Personal_IDType = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_IDType"]);
                                    pmrm.Member_Personal_IDNumber = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_IDNumber"]);
                                    pmrm.Member_Personal_Contact_Mobile_Number = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_Contact_Mobile_Number"]);
                                    pmrm.Member_Personal_Contact_Email = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_Contact_Email"]);
                                    pmrm.Member_Personal_Contact_Address1 = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_Contact_Address1"]);
                                    pmrm.Member_Personal_Contact_Address2 = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_Contact_Address2"]);
                                    pmrm.Member_Personal_Contact_Address3 = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_Contact_Address3"]);
                                    pmrm.Member_Personal_Contact_Others1 = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_Contact_Others1"]);
                                    pmrm.Member_Personal_Contact_Others2 = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_Contact_Others2"]);
                                    pmrm.Member_Personal_Contact_Others3 = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_Contact_Others3"]);
                                    pmrm.Member_Personal_Membership_Origin = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_Membership_Origin"]);
                                    pmrm.Member_Personal_Membership_Type = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_Membership_Type"]);
                                    pmrm.Member_Personal_Membership_Business_Entity = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_Membership_Business_Entity"]);
                                    pmrm.Member_Personal_Membership_Distributor_Number = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_Membership_Distributor_Number"]);
                                    pmrm.Member_Personal_Membership_JoinDate = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_Membership_JoinDate"]);
                                    pmrm.Member_Personal_Membership_Expiry = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_Membership_Expiry"]);
                                    pmrm.Member_Personal_Membership_Type2 = Convert.ToString(ds.Tables[1].Rows[i]["Member_Personal_Membership_Type2"]);
                                    pmrm.Member_Sponsor_Distributor_Number = Convert.ToString(ds.Tables[1].Rows[i]["Member_Sponsor_Distributor_Number"]);
                                    pmrm.Member_Sponsor_Name = Convert.ToString(ds.Tables[1].Rows[i]["Member_Sponsor_Name"]);
                                    pmrm.Member_Sponsor_Country = Convert.ToString(ds.Tables[1].Rows[i]["Member_Sponsor_Country"]);
                                    pmrm.Member_Spouse_FullName = Convert.ToString(ds.Tables[1].Rows[i]["Member_Spouse_FullName"]);
                                    pmrm.Member_Spouse_CitizenShip = Convert.ToString(ds.Tables[1].Rows[i]["Member_Spouse_CitizenShip"]);
                                    pmrm.Member_Spouse_DOB = Convert.ToString(ds.Tables[1].Rows[i]["Member_Spouse_DOB"]);
                                    pmrm.Member_Spouse_ID_Type = Convert.ToString(ds.Tables[1].Rows[i]["Member_Spouse_ID_Type"]);
                                    pmrm.Member_Spouse_ID_Number = Convert.ToString(ds.Tables[1].Rows[i]["Member_Spouse_ID_Number"]);
                                    pmrm.Member_Spouse_Photo = Convert.ToString(ds.Tables[1].Rows[i]["Member_Spouse_Photo"]);
                                    pmrm.Member_Spouse_MrgCertificate = Convert.ToString(ds.Tables[1].Rows[i]["Member_Spouse_MrgCertificate"]);
                                    pmrm.Member_Guardian_Name = Convert.ToString(ds.Tables[1].Rows[i]["Member_Guardian_Name"]);
                                    pmrm.Member_Guardian_RelationsShip = Convert.ToString(ds.Tables[1].Rows[i]["Member_Guardian_RelationsShip"]);
                                    pmrm.Member_Guardian_IDNumber = Convert.ToString(ds.Tables[1].Rows[i]["Member_Guardian_IDNumber"]);
                                    pmrm.Member_Guardian_ContactNumber = Convert.ToString(ds.Tables[1].Rows[i]["Member_Guardian_ContactNumber"]);
                                    pmrm.Member_CompanyName = Convert.ToString(ds.Tables[1].Rows[i]["Member_CompanyName"]);
                                    pmrm.Member_Company_Registration_Number = Convert.ToString(ds.Tables[1].Rows[i]["Member_Company_Registration_Number"]);
                                    pmrm.Member_Company_Corporation_Date = Convert.ToString(ds.Tables[1].Rows[i]["Member_Company_Corporation_Date"]);
                                    pmrm.Member_Company_Status = Convert.ToString(ds.Tables[1].Rows[i]["Member_Company_Status"]);
                                    pmrm.Member_Form_49 = Convert.ToString(ds.Tables[1].Rows[i]["Member_Form_49"]);
                                    pmrm.Member_Form_24 = Convert.ToString(ds.Tables[1].Rows[i]["Member_Form_24"]);
                                    pmrm.Member_Doctors_Resolution = Convert.ToString(ds.Tables[1].Rows[i]["Member_Doctors_Resolution"]);
                                    pmrm.Member_Bank_ACCNumber = Convert.ToString(ds.Tables[1].Rows[i]["Member_Bank_ACCNumber"]);
                                    pmrm.Member_Bank_ACC_Name_1 = Convert.ToString(ds.Tables[1].Rows[i]["Member_Bank_ACC_Name_1"]);
                                    pmrm.Member_Bank_ACC_IDCard_1 = Convert.ToString(ds.Tables[1].Rows[i]["Member_Bank_ACC_IDCard_1"]);
                                    pmrm.Member_Bank_ACC_Name_2 = Convert.ToString(ds.Tables[1].Rows[i]["Member_Bank_ACC_Name_2"]);
                                    pmrm.Member_Bank_ACC_IDCard_2 = Convert.ToString(ds.Tables[1].Rows[i]["Member_Bank_ACC_IDCard_2"]);
                                    pmrm.Member_Bank_ACC_Type = Convert.ToString(ds.Tables[1].Rows[i]["Member_Bank_ACC_Type"]);
                                    pmrm.Member_Bank_ACC_Name = Convert.ToString(ds.Tables[1].Rows[i]["Member_Bank_ACC_Name"]);
                                    pmrm.Member_Bank_ACC_Branch = Convert.ToString(ds.Tables[1].Rows[i]["Member_Bank_ACC_Branch"]);
                                    pmrm.Member_Bank_Statement_Header = Convert.ToString(ds.Tables[1].Rows[i]["Member_Bank_Statement_Header"]);
                                    pmrm.Member_Bank_Status = Convert.ToString(ds.Tables[1].Rows[i]["Member_Bank_Status"]);
                                    pmrm.Member_Language = Convert.ToString(ds.Tables[1].Rows[i]["Member_Language"]);
                                    lpmrm.Add(pmrm);
                                }
                                brm.Status = 1;
                                brm.Exception = null;
                                brm.Message = "Success";
                                brm.Payload = lpmrm;
                                brm.ValidationSummary = "";
                            }
                            else
                            {
                                brm.Status = 1;
                                brm.Exception = null;
                                brm.Message = "Success";
                                brm.Payload = null;
                                brm.ValidationSummary = Convert.ToString(ds.Tables[0].Rows[0][1]);
                            }
                        }
                        else
                        {
                            brm.Status = 0;
                            brm.Exception = null;
                            brm.Message = "Fail";
                            brm.Payload = null;
                            brm.ValidationSummary = Convert.ToString(ds.Tables[0].Rows[0][1]);
                        }
                    }
                    else
                    {
                        brm.Status = 0;
                        brm.Exception = "No Records Found.";
                        brm.Message = "Technical Error";
                        brm.Payload = null;
                        brm.ValidationSummary = "No Records Found.";
                    }
                }
                else
                {
                    brm.Status = 0;
                    brm.Exception = "No Records Found.";
                    brm.Message = "Technical Error";
                    brm.Payload = null;
                    brm.ValidationSummary = "No Records Found.";
                }

            }
            catch (Exception ex)
            {
                brm.Status = 0;
                brm.Exception = ex.Message.ToString();
                brm.Message = "Technical Error";
                brm.Payload = null;
                brm.ValidationSummary = null;
            }
            return brm;
        }
    }
}
