﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
 
using eSH.Model.Api.Common;
using eSH.Model.Api.Request;
using eSH.Model.Api.Response;
using eshWebAPI.Models.eSH.Data.CommonUtility;

namespace eSH.Data.Repositories
{
  public class RegistrationRepository
    {
       CommonUtility cu = new  CommonUtility();

        public BaseResponseModel ManageMemberRegistrations(RegistrationRequestInformation rri)
        {
            RegistrationResponseModel rrm = new RegistrationResponseModel();
            List<RegistrationResponseModel> lrrm = new List<RegistrationResponseModel>();
            BaseResponseModel brm = new BaseResponseModel();

            try
            {
                SqlParameter[] param = new SqlParameter[43];

                param[0] = new SqlParameter("@Member_PK", rri.Member_PK);
                param[1] = new SqlParameter("@Membership_Origin", rri.Membership_Origin);
                param[2] = new SqlParameter("@Country_OF_CitizenShip", rri.Country_OF_CitizenShip);
                param[3] = new SqlParameter("@isPR", rri.isPR);
                param[4] = new SqlParameter("@Membership_Type", rri.Membership_Type);
                param[5] = new SqlParameter("@Register_as_BusinessEntity", rri.Register_as_BusinessEntity);
                param[6] = new SqlParameter("@Member_Email_Address", rri.Member_Email_Address);
                param[7] = new SqlParameter("@Member_Mobile_Number", rri.Member_Mobile_Number);
                param[8] = new SqlParameter("@Member_Full_Name", rri.Member_Full_Name);
                param[9] = new SqlParameter("@Member_Gender", rri.Member_Gender);
                param[10] = new SqlParameter("@Member_Ethinicity", rri.Member_Ethinicity);
                param[11] = new SqlParameter("@Member_Date_OF_Birth", rri.Member_Date_OF_Birth);
                param[12] = new SqlParameter("@Guardian_Name", rri.Guardian_Name);
                param[13] = new SqlParameter("@Guardian_Relationship", rri.Guardian_Relationship);
                param[14] = new SqlParameter("@Guardian_Identity_Card", rri.Guardian_Identity_Card);
                param[15] = new SqlParameter("@Guardian_Contact_Number", rri.Guardian_Contact_Number);
                param[16] = new SqlParameter("@Applicant_Identity_Type", rri.Applicant_Identity_Type);
                param[17] = new SqlParameter("@Identity_Card_Number", rri.Identity_Card_Number);
                param[18] = new SqlParameter("@Member_Address_1", rri.Member_Address_1);
                param[19] = new SqlParameter("@Member_Address_2", rri.Member_Address_2);
                param[20] = new SqlParameter("@Member_Address_3", rri.Member_Address_3);
                param[21] = new SqlParameter("@Member_Town", rri.Member_Town);
                param[22] = new SqlParameter("@Member_State", rri.Member_State);
                param[23] = new SqlParameter("@Member_Postal", rri.Member_Postal);
                param[24] = new SqlParameter("@Member_RukunTetangga", rri.Member_RukunTetangga);
                param[25] = new SqlParameter("@Member_RukunWarga", rri.Member_RukunWarga);
                param[26] = new SqlParameter("@Member_Language_Preference", rri.Member_Language_Preference);
                param[27] = new SqlParameter("@isSpouse", rri.isSpouse);
                param[28] = new SqlParameter("@Spouse_Full_Name", rri.Spouse_Full_Name);
                param[29] = new SqlParameter("@Spouse_Nationality", rri.Spouse_Nationality);
                param[30] = new SqlParameter("@Spouse_Date_Of_Birth", rri.Spouse_Date_Of_Birth);
                param[31] = new SqlParameter("@Spouse_Ethinicity", rri.Spouse_Ethinicity);
                param[32] = new SqlParameter("@Spouse_Applicant_Identity_Type", rri.Spouse_Applicant_Identity_Type);
                param[33] = new SqlParameter("@Spouse_Identity_Card_Number", rri.Spouse_Identity_Card_Number);
                param[34] = new SqlParameter("@Spouse_Mobile_Number", rri.Spouse_Mobile_Number);
                param[35] = new SqlParameter("@isRegisterationDone", rri.isRegisterationDone);
                param[36] = new SqlParameter("@Sponsor_Member_Parent_FK", rri.Sponsor_Member_Parent_FK);
                param[37] = new SqlParameter("@isTCConfirmed", rri.isTCConfirmed);
                param[38] = new SqlParameter("@isDeleted", rri.isDeleted);
                param[39] = new SqlParameter("@isApproved", rri.isApproved);
                param[40] = new SqlParameter("@isActive", rri.isActive);
                param[41] = new SqlParameter("@Login_FK", rri.LoginID);
                param[42] = new SqlParameter("@Flag", rri.Flag);

                DataSet ds = DataBaseAccessLayer.Select(CommonSP.usp_ManageRegistrations, param);

                if (cu.checkDatasetRecords(ds))
                {
                    if (cu.checkDataTableRecords(ds.Tables[0]))
                    {
                        if (ds.Tables[0].Rows[0][0].ToString() == "1")
                        {
                            if (ds.Tables.Count > 1 && cu.checkDataTableRecords(ds.Tables[1]))
                            {
                                //in case of GET records it will come.
                                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                                {
                                    rrm = new RegistrationResponseModel();
                                    rrm.Member_PK = Convert.ToInt32(ds.Tables[1].Rows[i]["Member_PK"]);
                                    rrm.Registration_Number = Convert.ToString(ds.Tables[1].Rows[i]["Registration_Number"]);
                                    lrrm.Add(rrm);
                                }
                                brm.Status = 1;
                                brm.Exception = null;
                                brm.Message = "Success";
                                brm.Payload = lrrm;
                                brm.ValidationSummary = "";
                            }
                            else
                            {
                                brm.Status = 1;
                                brm.Exception = null;
                                brm.Message = "Success";
                                brm.Payload = null;
                                brm.ValidationSummary = Convert.ToString(ds.Tables[0].Rows[0][1]);
                            }
                        }
                        else
                        {
                            brm.Status = 0;
                            brm.Exception = null;
                            brm.Message = "Fail";
                            brm.Payload = null;
                            brm.ValidationSummary = Convert.ToString(ds.Tables[0].Rows[0][1]);
                        }
                    }
                    else
                    {
                        brm.Status = 0;
                        brm.Exception = "No Records Found.";
                        brm.Message = "Technical Error";
                        brm.Payload = null;
                        brm.ValidationSummary = "No Records Found.";
                    }
                }
                else
                {
                    brm.Status = 0;
                    brm.Exception = "No Records Found.";
                    brm.Message = "Technical Error";
                    brm.Payload = null;
                    brm.ValidationSummary = "No Records Found.";
                }

            }
            catch (Exception ex)
            {
                brm.Status = 0;
                brm.Exception = ex.Message.ToString();
                brm.Message = "Technical Error";
                brm.Payload = null;
                brm.ValidationSummary = null;
            }
            return brm;
        }
    }
}
