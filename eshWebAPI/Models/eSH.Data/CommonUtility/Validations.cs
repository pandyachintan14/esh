﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using static eshWebAPI.Models.eSH.Data.CommonUtility.Variables;

namespace eshWebAPI.Models.eSH.Data.CommonUtility
{
    /// <summary>
    /// it provides validations to various model objects
    /// </summary>
    public static class Validations
    {
        /// <summary>
        /// This method validate properties values to various server side validations.
        /// Please Make sure When ever User wants to test with Regex 
        /// please pass dictionary object value with in dictionary so that it can validate multiple regex and please pass message with regex so that it can return if any failures occur. 
        /// </summary>
        /// <param name="displayTitle">Field Name which is display to user</param>
        /// <param name="value">Value of Field</param>
        /// <param name="validationCheckList">All Possible validations need to perform on fields.</param>
        /// <param name="validationSummary">Out parameter which returns validation summary list.</param>
        /// <returns>true or false</returns>
        public static Boolean ValidateValues(object displayTitle, object value, Dictionary<object, object> validationCheckList, out List<string> validationSummary)
        {
            List<string> validationMessageSummary = new List<string>();

            foreach (var item in validationCheckList)
            {

                if ((ModelPropertiesValidations)item.Key == ModelPropertiesValidations.isInt)
                {
                    int intvalue = 0;
                    if (!int.TryParse(Convert.ToString(value), out intvalue))
                    {
                        validationMessageSummary.Add("* Field " + displayTitle + " requires numeric value only. Please enter valid numeric value in " + displayTitle + ".");
                    }
                }
                else if ((ModelPropertiesValidations)item.Key == ModelPropertiesValidations.isBoolean)
                {
                    Boolean boolean = false;
                    if (!Boolean.TryParse(Convert.ToString(value), out boolean))
                    {
                        validationMessageSummary.Add("* Field " + displayTitle + " requires boolean value. Please enter valid boolean value in " + displayTitle + ".");
                    }
                }
                else if ((ModelPropertiesValidations)item.Key == ModelPropertiesValidations.isNotContains)
                {

                    if (Convert.ToString(value).Contains(Convert.ToString(item.Value)))
                    {
                        validationMessageSummary.Add("* Field " + displayTitle + " contains " + Convert.ToString(item.Value) + ". Please provide correct input.");
                    }
                }
                else if ((ModelPropertiesValidations)item.Key == ModelPropertiesValidations.isDateTime)
                {
                    DateTime datetime = DateTime.Now;
                    if (!DateTime.TryParse(Convert.ToString(value), out datetime))
                    {
                        validationMessageSummary.Add("* Field " + displayTitle + " Date Type Field. Please provide correct input.");
                    }
                }
                else if ((ModelPropertiesValidations)item.Key == ModelPropertiesValidations.isDecimal)
                {
                    decimal decimalValue = 0;
                    if (!decimal.TryParse(Convert.ToString(value), out decimalValue))
                    {
                        validationMessageSummary.Add("* Field " + displayTitle + " Decimal Type Field. Please provide correct input.");
                    }
                }
                else if ((ModelPropertiesValidations)item.Key == ModelPropertiesValidations.isMaxlength)
                {

                    if (!(Convert.ToString(value).Length < Convert.ToInt32(item.Value)))
                    {
                        validationMessageSummary.Add("* Field " + displayTitle + " requires minimum length of " + Convert.ToInt32(item.Value).ToString() + "." + " Please enter valid input.");
                    }
                }
                else if ((ModelPropertiesValidations)item.Key == ModelPropertiesValidations.isNullorEmptyorRequired)
                {

                    if ((string.IsNullOrEmpty(Convert.ToString(value))))
                    {
                        validationMessageSummary.Add("* Field " + displayTitle + " is required field can not be blank.");
                    }
                }
                else if ((ModelPropertiesValidations)item.Key == ModelPropertiesValidations.isRegex)
                {
                    foreach (var inneritem in (Dictionary<object, object>)item.Value)
                    {
                        Regex reg = new Regex(Convert.ToString(inneritem.Key), RegexOptions.IgnoreCase);
                        if (!reg.IsMatch(Convert.ToString(value)))
                        {
                            validationMessageSummary.Add(Convert.ToString(inneritem.Value));
                        }
                    }
                }
            }
            validationSummary = validationMessageSummary;

            if (validationMessageSummary.Count > 0)
                return false;
            else
                return true;
        }
    }
}
