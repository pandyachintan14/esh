﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
 
using System.Configuration;
 
using System.Linq;
using System.Reflection;
 
using System.Threading.Tasks;
using System.Web;
namespace eshWebAPI.Models.eSH.Data.CommonUtility
{
    /// <summary>
    /// This Class is usefull for Database related stuff. this supports Insert / Update / Select Methods
    /// </summary>
    /// <example>
    /// <code>
    ///  public string GetTestInsert()
    ///  {
    ///      SqlParameter[] param = {
    ///      new SqlParameter("@p1","test12dfsfjhsjkfh "),
    ///      new SqlParameter("@p2","12345564668"),
    ///
    ///  };
    ///
    ///        bool result;
    ///        string error = string.Empty;
    ///
    ///     DAL.InsertUpdate("testInsert", param, out result, out error);
    ///
    ///     //DAL.InsertUpdate("testUpdate", null, out result, out error);
    ///
    ///
    ///     DataSet ds = new DataSet();
    ///     ds = DAL.Select("testSelect", null, out result, out error);
    ///     DataTable dt = new DataTable();
    ///      dt = DAL.Select("testSelect", null, 0,out result,out error);
    ///
    ///  
    ///
    ///      return error;
    ///     } 
    /// 
    ///    public static DataTable Select(string storedProcedure, SqlParameter[] param, int tableIndexNumber, out bool result)
    ///            {
    ///                using (SqlConnection cn = new SqlConnection(ConStr))
    ///                {
    ///                    try
    ///                    {
    ///                        using (SqlCommand cmd = new SqlCommand(storedProcedure, cn))
    ///                        {
    ///                            cmd.CommandType = CommandType.StoredProcedure;
    ///                            if (param != null)
    ///                            {
    ///                                foreach (SqlParameter p in param)
    ///                                {
    ///                                    cmd.Parameters.Add(p);
    ///                                }
    ///                            }
    ///                            DataSet ds = new DataSet();
    ///              SqlDataAdapter adpt = new SqlDataAdapter(cmd);
    ///              adpt.Fill(ds);
    ///                            if (ds != null and ds.Tables.Count > 0)
    ///                            {
    ///                                if (ds.Tables.Count > tableIndexNumber)
    ///                                {
    ///                                    result = true;
    ///                                    return ds.Tables[tableIndexNumber];
    ///                                }
    ///                                else
    ///                                {
    ///                                    result = false;
    ///                                    return new DataTable();
    ///                                }
    ///                            }
    ///                            else
    ///                            {
    ///                                result = false;
    ///                                return new DataTable();
    ///                            }
    ///                        }
    ///                    }
    ///                    catch (Exception ex)
    ///                    {
    ///                        
    ///                        result = false;
    ///                        
    ///                     //How Insert Error is used.
    ///                        InsertError(null, MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace.ToString(), ex.Source.ToString(), "Framework", null, storedProcedure, -1);
    ///                        return new DataTable();
    ///                    }
    ///                    finally
    ///                    {
    ///                        cn.Close();
    ///                        cn.Dispose();
    ///                    }
    ///                }
    ///            }
    ///            </code>
    ///
    /// </example>
    public static class DataBaseAccessLayer
    { /// <summary>
      /// ConStr is Variable which stores Connection String from Web Cconfig file.
      /// </summary>
        //public static string ConStr = @"Data Source=DESKTOP-MET2U5M\ULTIMATE;Initial Catalog=esh_db;Integrated Security=True";
        
        public static string ConStr = @"Data Source=eshdb.cql8ufcrlwrc.us-east-2.rds.amazonaws.com,1433;Initial Catalog=esh_db;Persist Security Info=True;User ID=admin;Password=admin123";

        #region Check Database Connection
        /// <summary>
        /// Check Database Connection
        /// </summary>
        /// <returns>String value for Success /Error Messages </returns>
        public static string checkConnection()
        {
            try
            {
                using (SqlConnection cn = new SqlConnection(ConStr))
                {
                    try
                    {
                        cn.Open();
                        return CommonMessage.success;
                    }
                    catch (Exception ex)
                    { return ex.Message; }
                    finally
                    {
                        cn.Close();
                    }
                }

            }
            catch (Exception ex)
            { return ex.Message; }
        }
        #endregion

        #region insert data to database
        ///<summary>This function is usefull for insert/Update data
        ///</summary>
        ///<param name="storedProcedure">
        /// Write Stored Procedure Name
        /// </param>
        /// <param name="param">
        /// Pass SqlParameter array
        /// </param>
        /// <param name="result">
        /// Get boolean value for record insert or not
        /// </param>
        /// <param name="error">
        /// Get detailed error description from this parameter
        /// </param>
        public static void InsertUpdate(string storedProcedure, SqlParameter[] param, out bool result, out string error)
        {
            using (SqlConnection cn = new SqlConnection(ConStr))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand(storedProcedure, cn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (SqlParameter p in param)
                            {
                                cmd.Parameters.Add(p);
                            }
                        }
                        cn.Open();
                        cmd.ExecuteNonQuery();
                        result = true;
                        error = CommonMessage.NoError;
                    }

                }
                catch (Exception ex)
                {
                    result = false;
                    error = ex.Message;
                    InsertError(null, MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace.ToString(), ex.Source.ToString(), "Framework", null, storedProcedure, -1);
                }
                finally
                { cn.Close(); }
            }
        }
        /// <summary>
        /// This function is overloaded method. which supports Insert/ Update with database.
        /// </summary>
        /// <param name="storedProcedure">Write Stored Procedure Name</param>
        /// <param name="param">Pass SqlParameter array</param>
        /// <param name="result">Get boolean value for record insert or not</param>
        public static void InsertUpdate(string storedProcedure, SqlParameter[] param, out bool result)
        {
            using (SqlConnection cn = new SqlConnection(ConStr))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand(storedProcedure, cn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (SqlParameter p in param)
                            {
                                cmd.Parameters.Add(p);
                            }
                        }
                        cn.Open();
                        cmd.ExecuteNonQuery();
                        result = true;
                    }

                }
                catch (Exception ex)
                {
                    result = false;
                    InsertError(null, MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace.ToString(), ex.Source.ToString(), "Framework", null, storedProcedure, -1);
                }
                finally
                { cn.Close(); }
            }
        }
        /// <summary>
        /// This function is overloaded method. which supports Insert/ Update with database.
        /// </summary>
        /// <param name="storedProcedure">Write Stored Procedure Name</param>
        /// <param name="param">Pass SqlParameter array</param>
        public static void InsertUpdate(string storedProcedure, SqlParameter[] param)
        {
            using (SqlConnection cn = new SqlConnection(ConStr))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand(storedProcedure, cn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (SqlParameter p in param)
                            {
                                cmd.Parameters.Add(p);
                            }
                        }
                        cn.Open();
                        cmd.ExecuteNonQuery();
                    }

                }
                catch (Exception ex)
                {
                    InsertError(null, MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace.ToString(), ex.Source.ToString(), "Framework", null, storedProcedure, -1);
                }
                finally
                { cn.Close(); }
            }
        }

        #endregion

        #region select dataset from database
        /// <summary>
        /// This function using for select dataset from database and return value in dataset
        /// </summary>
        /// <param name="storedProcedure">Write stored procedure name</param>
        /// <param name="param">Pass parameter if any otherwise pass NULL</param>
        /// <param name="result">Return result value in true/false, If dataset null or no any table contain then result return false value</param>
        /// <param name="error">Get detailed error description in this parameter</param>
        /// <returns>Dataset Object</returns>
        public static DataSet Select(string storedProcedure, SqlParameter[] param, out bool result, out string error)
        {
            using (SqlConnection cn = new SqlConnection(ConStr))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand(storedProcedure, cn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (SqlParameter p in param)
                            {
                                cmd.Parameters.Add(p);
                            }
                        }
                        DataSet ds = new DataSet();
                        SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                        adpt.Fill(ds);
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            error = CommonMessage.NoError;
                            result = true;
                            return ds;
                        }
                        else
                        {
                            error = CommonMessage.NullDataSet;
                            result = false;
                            return ds;
                        }
                    }
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    result = false;
                    InsertError(null, MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace.ToString(), ex.Source.ToString(), "Framework", null, storedProcedure, -1);
                    return new DataSet();
                }
                finally
                {
                    cn.Close();
                    cn.Dispose();
                }
            }
        }
        /// <summary>
        /// This function using for select dataset from database and return value in dataset
        /// </summary>
        /// <param name="storedProcedure">Write stored procedure name</param>
        /// <param name="param">Pass parameter if any otherwise pass NULL</param>
        /// <param name="result">Return result value in true/false, If dataset null or no any table contain then result return false value</param>
        /// <returns>Dataset Object</returns>
        public static DataSet Select(string storedProcedure, SqlParameter[] param, out bool result)
        {
            using (SqlConnection cn = new SqlConnection(ConStr))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand(storedProcedure, cn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (SqlParameter p in param)
                            {
                                cmd.Parameters.Add(p);
                            }
                        }
                        DataSet ds = new DataSet();
                        SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                        adpt.Fill(ds);
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            result = true;
                            return ds;
                        }
                        else
                        {
                            result = false;
                            return ds;
                        }
                    }
                }
                catch (Exception ex)
                {
                    result = false;
                    InsertError(null, MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace.ToString(), ex.Source.ToString(), "Framework", null, storedProcedure, -1);
                    return new DataSet();
                }
                finally
                {
                    cn.Close();
                    cn.Dispose();
                }
            }
        }
        /// <summary>
        /// This function using for select dataset from database and return value in dataset
        /// </summary>
        /// <param name="storedProcedure">Write stored procedure name</param>
        /// <param name="param">Pass parameter if any otherwise pass NULL</param>
        /// <returns>Dataset Object</returns>
        public static DataSet Select(string storedProcedure, SqlParameter[] param)
        {
            using (SqlConnection cn = new SqlConnection(ConStr))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand(storedProcedure, cn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (SqlParameter p in param)
                            {
                                cmd.Parameters.Add(p);
                            }
                        }
                        DataSet ds = new DataSet();
                        SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                        adpt.Fill(ds);
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            return ds;
                        }
                        else
                        {
                            return ds;
                        }
                    }
                }
                catch (Exception ex)
                {
                    InsertError(null, MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace.ToString(), ex.Source.ToString(), "Framework", null, storedProcedure, -1);
                    return new DataSet();
                }
                finally
                {
                    cn.Close();
                    cn.Dispose();
                }
            }
        }

        #endregion

        #region select dataTable from database
        /// <summary>
        /// This function using for select datatable from database and return value in dataset
        /// </summary>
        /// <param name="storedProcedure">Write stored procedure name</param>
        /// <param name="param">Pass parameter if any otherwise pass NULL</param>
        /// <param name="result">Return result value in true/false, If dataset null or no any table contain then result return false value</param>
        /// <param name="error">Get detailed error description in this parameter</param>
        /// <param name="tableIndexNumber">Pass table index number, its starting from 0[Zero]</param>
        /// <returns>Datatable Object</returns>
        public static DataTable Select(string storedProcedure, SqlParameter[] param, int tableIndexNumber, out bool result, out string error)
        {
            using (SqlConnection cn = new SqlConnection(ConStr))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand(storedProcedure, cn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (SqlParameter p in param)
                            {
                                cmd.Parameters.Add(p);
                            }
                        }
                        DataSet ds = new DataSet();
                        SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                        adpt.Fill(ds);
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            if (ds.Tables.Count > tableIndexNumber)
                            {
                                error = CommonMessage.NoError;
                                result = true;
                                return ds.Tables[tableIndexNumber];
                            }
                            else
                            {
                                error = CommonMessage.NoTable(tableIndexNumber);
                                result = false;
                                return new DataTable();
                            }
                        }
                        else
                        {
                            error = CommonMessage.NullDataSet;
                            result = false;
                            return new DataTable();
                        }
                    }
                }
                catch (Exception ex)
                {
                    error = ex.Message;
                    result = false;
                    InsertError(null, MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace.ToString(), ex.Source.ToString(), "Framework", null, storedProcedure, -1);
                    return new DataTable();
                }
                finally
                {
                    cn.Close();
                    cn.Dispose();
                }
            }
        }
        /// <summary>
        /// This function using for select datatable from database and return value in dataset
        /// </summary>
        /// <param name="storedProcedure">Write stored procedure name</param>
        /// <param name="param">Pass parameter if any otherwise pass NULL</param>
        /// <param name="result">Return result value in true/false, If dataset null or no any table contain then result return false value</param>
        /// <param name="tableIndexNumber">Pass table index number, its starting from 0[Zero]</param>
        /// <returns>Datatable Object</returns>
        public static DataTable Select(string storedProcedure, SqlParameter[] param, int tableIndexNumber, out bool result)
        {
            using (SqlConnection cn = new SqlConnection(ConStr))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand(storedProcedure, cn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (SqlParameter p in param)
                            {
                                cmd.Parameters.Add(p);
                            }
                        }
                        DataSet ds = new DataSet();
                        SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                        adpt.Fill(ds);
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            if (ds.Tables.Count > tableIndexNumber)
                            {
                                result = true;
                                return ds.Tables[tableIndexNumber];
                            }
                            else
                            {
                                result = false;
                                return new DataTable();
                            }
                        }
                        else
                        {
                            result = false;
                            return new DataTable();
                        }
                    }
                }
                catch (Exception ex)
                {

                    result = false;
                    InsertError(null, MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace.ToString(), ex.Source.ToString(), "Framework", null, storedProcedure, -1);
                    return new DataTable();
                }
                finally
                {
                    cn.Close();
                    cn.Dispose();
                }
            }
        }

        /// <summary>
        /// This function using for select datatable from database and return value in dataset
        /// </summary>
        /// <param name="storedProcedure">Write stored procedure name</param>
        /// <param name="param">Pass parameter if any otherwise pass NULL</param>
        /// <param name="tableIndexNumber">Pass table index number, its starting from 0[Zero]</param>
        /// <returns>Datatable Object</returns>
        public static DataTable Select(string storedProcedure, SqlParameter[] param, int tableIndexNumber)
        {
            using (SqlConnection cn = new SqlConnection(ConStr))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand(storedProcedure, cn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        if (param != null)
                        {
                            foreach (SqlParameter p in param)
                            {
                                cmd.Parameters.Add(p);
                            }
                        }
                        DataSet ds = new DataSet();
                        SqlDataAdapter adpt = new SqlDataAdapter(cmd);
                        adpt.Fill(ds);
                        if (ds != null && ds.Tables.Count > 0)
                        {
                            if (ds.Tables.Count > tableIndexNumber)
                            {
                                return ds.Tables[tableIndexNumber];
                            }
                            else
                            {
                                return new DataTable();
                            }
                        }
                        else
                        {
                            return new DataTable();
                        }
                    }
                }
                catch (Exception ex)
                {
                    InsertError(null, MethodBase.GetCurrentMethod().Name, ex.Message, ex.StackTrace.ToString(), ex.Source.ToString(), "Framework", null, storedProcedure, -1);
                    return new DataTable();
                }
                finally
                {
                    cn.Close();
                    cn.Dispose();
                }
            }
        }
        #endregion
        /// <summary>
        /// This method inserts Error Log in Database.
        /// </summary>
        /// <param name="ControllerName">Name of Controller</param>
        /// <param name="MethodName">Name of Method</param>
        /// <param name="ErrorDescription">Error Description</param>
        /// <param name="ErrorStachTrace">Error Trace Details</param>
        /// <param name="ErrorSource">Error Source Details</param>
        /// <param name="ErrorFrom">Error From Details</param>
        /// <param name="ClientIPAddress">Client IP Address</param>
        /// <param name="StoredProcedureName">Stored Procedure Name</param>
        /// <param name="EmpLoginFK">Employee Login PK Name</param>

        public static void InsertError(string ControllerName, string MethodName, string ErrorDescription, string ErrorStachTrace, string ErrorSource, string ErrorFrom, string ClientIPAddress, string StoredProcedureName, int EmpLoginFK = -1)
        {
            using (SqlConnection cn = new SqlConnection(ConStr))
            {
                try
                {
                    using (SqlCommand cmd = new SqlCommand(CommonSP.ErrorSP, cn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;

                        SqlParameter[] param = new SqlParameter[] {
                            new SqlParameter("@ControllerName",string.IsNullOrEmpty(ControllerName) ? "" : ControllerName),
                            new SqlParameter("@MethodName",string.IsNullOrEmpty(MethodName) ? "" : MethodName),
                            new SqlParameter("@ErrorDescription",string.IsNullOrEmpty(ErrorDescription) ? "" : ErrorDescription),
                            new SqlParameter("@ErrorStackTrace",string.IsNullOrEmpty(ErrorStachTrace) ? "" : ErrorStachTrace),
                            new SqlParameter("@ErrorSource",string.IsNullOrEmpty(ErrorSource) ? "" : ErrorSource),
                            new SqlParameter("@ErrorFrom",string.IsNullOrEmpty(ErrorFrom) ? "" : ErrorFrom),
                            new SqlParameter("@EmpLoginFK",EmpLoginFK) 
                            
                        };
                        cmd.Parameters.AddRange(param);
                        cn.Open();
                        cmd.ExecuteNonQuery();



                    }
                }
                catch (Exception ex)
                { throw ex; }
                finally
                { cn.Close(); }
            }
        }


    }
}
