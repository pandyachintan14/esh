﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eshWebAPI.Models.eSH.Data.CommonUtility
{
    public static class CommonSP
    {
        /// <summary>
        /// This SP using for save Error Description in Framework
        /// </summary>
        public static string ErrorSP = "FR_InsertError";

        /// <summary>
        /// This SP using for Manage EMail Log
        /// </summary>
        public static string EmailLogSP = "manageEmaillog";

        /// <summary>
        /// This SP using for Manage Upload or Download Log
        /// </summary>
        public static string UploadDownloadLogSP = "UploadDownloadLog";

        /// <summary>
        /// This SP Manages Registrations
        /// </summary>
        public static string usp_ManageRegistrations = "usp_ManageRegistrations";

        /// <summary>
        /// This SP Get Sponser Details
        /// </summary>
        public static string usp_GetSponserDetails = "usp_GetSponserDetails";
        /// <summary>
        /// This SP Manages Member's Approval/Rejections/T&C/Checkout
        /// </summary>
        public static string usp_ManagesMemberApproval = "usp_ManagesMemberApproval";

        /// <summary>
        /// This SP Get Profile Details
        /// </summary>
        public static string usp_GetMemberProfileDetails = "usp_GetMemberProfileDetails";
        /// <summary>
        /// This SP for Manage User Profile
        /// </summary>
        public static string SP_ManageUserPersonalInformation = "SP_ManageUserPersonalInformation";
        /// <summary>
        /// This SP Get Geneology Tree Details
        /// </summary>
        public static string usp_GetGeneologyTreeDetails = "usp_GetGeneologyTreeDetails";

        /// <summary>
        /// This SP for Login
        /// </summary>
        public static string usp_CheckLogin = "usp_CheckLogin";

        /// <summary>
        /// This SP for Manage Password
        /// </summary>
        public static string usp_ManagePassword = "usp_ManagePassword";

       
    }
}
