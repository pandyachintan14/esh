﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace eshWebAPI.Models.eSH.Data.CommonUtility
{
    public class CommonUtility
    {
        /// <summary>
        /// Check Dataset Records
        /// </summary>
        /// <param name="ds">Dataset object</param>
        /// <returns></returns>
        public Boolean checkDatasetRecords(DataSet ds)
        {
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                return true;
            else
                return false;
        }
        /// <summary>
        /// Check Data Table Records
        /// </summary>
        /// <param name="ds">Data Table object</param>
        /// <returns></returns>
        public Boolean checkDataTableRecords(DataTable dt)
        {
            if (dt != null && dt.Rows.Count > 0)
                return true;
            else
                return false;
        }
    }
}
