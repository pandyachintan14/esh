﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eshWebAPI.Models.eSH.Data.CommonUtility
{
    /// <summary>
    /// it provides variables to various methods such as enum, static values, Codes etc.
    /// </summary>
    public static class Variables
    {
        public enum APIResponseStatus
        {
            FailDueToError = 0,
            Success = 1,
            ValidationFailure = 2,
            AuthorizationFail = 3
        }
        public enum ModelPropertiesValidations
        {
            isNullorEmptyorRequired = 0,
            isMaxlength = 1,
            isInt = 2,
            isDateTime = 3,
            isBoolean = 4,
            isRegex = 5,
            isNotContains = 6,
            isDecimal = 7
        }
    }
}
