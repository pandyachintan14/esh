﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace eshWebAPI.Models.eSH.Data.CommonUtility
{
    public static class FileUpload
    {
        public static string ConvertBase64ToImg(string base64string, string FilePath, string FileName)
        {

            //Convert Base64 Encoded string to Byte Array.
            byte[] imageBytes = Convert.FromBase64String(base64string);

            //Save the Byte Array as Image File.

            if (!Directory.Exists(FilePath))
            {
                Directory.CreateDirectory(FilePath);
            }

            string filePath = FilePath + "/" + FileName;
            File.WriteAllBytes(filePath, imageBytes);
            return FilePath;
        }
    }
}
