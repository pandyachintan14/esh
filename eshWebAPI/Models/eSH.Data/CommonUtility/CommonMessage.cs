﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eshWebAPI.Models.eSH.Data.CommonUtility
{
    /// <summary>
    /// This class provides Common Messages for various usage.
    /// </summary>
    public static class CommonMessage
    {
        /// <summary>
        /// Message - No Error
        /// </summary>
        public static string NoError = "No Error";

        /// <summary>
        /// Message - Error Occured
        /// </summary>
        public static string Error = "Error Occurred";

        /// <summary>
        /// Message - Login Successfully
        /// </summary>
        public static string LoginSuccess = "Login Successfully";

        /// <summary>
        /// Message - Success
        /// </summary>
        public static string success = "Success";

        /// <summary>
        /// Message - NULL dataset or No any table in dataset
        /// </summary>
        public static string NullDataSet = "NULL dataset or No any table in dataset";

        /// <summary>
        /// Message - Dataset not contain any table on 5 index
        /// </summary>
        /// <param name="tableIndex">
        /// Pass table index number
        /// </param>
        /// <returns></returns>
        public static string NoTable(int tableIndex)
        {
            return "Dataset not contain any table on " + tableIndex.ToString() + " index";
        }

        /// <summary>
        /// Message - TO, CC or BCC has no any email address. Please pass atleast one parameter value.
        /// </summary>
        public static string RequiredEmail = "TO, CC or BCC has no any email address. Please pass atleast one parameter value.";

        /// <summary>
        /// Message - error occured while conversion.
        /// </summary>
        public static string ConversionError = "Error occured while conversion, Please set out parameter for get details error.";

        /// <summary>
        /// Message - Error occured during encryption, Please set out parameter for get details error.
        /// </summary>
        public static string EncryptionError = "Error occurred during encryption, Please set out parameter for get details error.";

        /// <summary>
        /// Message - Error occured during decryption, Please set out parameter for get details error.
        /// </summary>
        public static string DecryptionError = "Error occurred during decryption, Please set out parameter for get details error.";

        /// <summary>
        /// Message - Invalid Username or Password, Please try again.
        /// </summary>
        public static string InvalidLogin = "Invalid Username or Password, Please try again.";

        /// <summary>
        /// Message - You are not authorized person to access this system from outside premises
        /// </summary>
        public static string OutsideLogin = "You are not authorized person to access this system from outside premises";

        /// <summary>
        /// Message - You have reached maximum attempts for login.
        /// </summary>
        public static string InvalidTry = "You have reached maximum attempts for login.";

        /// <summary>
        /// Message - Your account is locked and it will be open at 12:00 PM today.
        /// </summary>
        public static string LockLogin = "Your account is locked and it will be open at 12:00 PM today.";

        /// <summary>
        /// Message - Your Password reset successfully
        /// </summary>
        public static string ResetPassword = "Your Password reset successfully";

        /// <summary>
        /// Message - Password and Confirm password is different, Please enter same password.
        /// </summary>
        public static string ConfirmPassword = "Password and Confirm password is different, Please enter same password.";

        /// <summary>
        /// Message - Your account is deactivate
        /// </summary>
        public static string deactivateLogin = "Your account is deactivate";

        /// <summary>
        /// Message - Your login account closed.
        /// </summary>
        public static string LoginClosed = "Your login account closed.";

        /// <summary>
        /// Message - Username already exists, Please enter another username.
        /// </summary>
        public static string sameUsername = "Username already exists, Please enter different username.";

        /// <summary>
        /// Message - Username is available
        /// </summary>
        public static string validUsername = "Username is available";

        /// <summary>
        /// Message - Employee code is available
        /// </summary>
        public static string validEmpCode = "Employee code is available";

        /// <summary>
        /// Message - Employee code already exists, Please enter unique employee code
        /// </summary>
        public static string sameEmpCode = "Employee code already exists, Please enter unique employee code";

        /// <summary>
        /// Message - File not uploaded.
        /// </summary>
        public static string FileNotFound = "File Not Found";

        /// <summary>
        /// Message - Login Created Successfully.
        /// </summary>
        public static string CreateLogin = "Login Created Successfully.";

        /// <summary>
        /// Message - You need to reset password for your login.
        /// </summary>
        public static string needReset = "You need to reset password for your login.";

        /// <summary>
        /// Message - Your password not reset, Please try again.
        /// </summary>
        public static string noReset = "Your password not reset, Please try again.";

        /// <summary>
        /// Message - Old password not match, Please re-enter your old password.
        /// </summary>
        public static string oPassNotMatch = "Old password not match, Please re-enter your old password.";

        /// <summary>
        /// Message - Save In parameter not valid.
        /// </summary>
        public static string InvalidParameter = "Save In parameter not valid.";

        /// <summary>
        /// Message - Save In parameter not valid.
        /// </summary>
      //  public static string FileSizeExceed = "You cannot upload more than " + Convert.ToInt32(Convert.ToDouble(ConfigurationManager.AppSettings["FileSize"]) / (1024 * 1024)) + " MB file size.";

        /// <summary>
        /// Message - Data Inseted Successfully.
        /// </summary>
        public static string Inserted = "Data Inserted Successfully.";

        /// <summary>
        /// Message - Data Updated Successfully.
        /// </summary>
        public static string Updated = "Data Updated Successfully.";

        /// <summary>
        /// Message - Data Already Exists.
        /// </summary>
        public static string AlreadyExist = "Data Already Exists.";

        /// <summary>
        /// Message - Username is invalid, Please enter correct username
        /// </summary>
        public static string InvalidUsername = "Username is invalid, Please enter correct username";

        /// <summary>
        /// Message - Your new password has been sent to your registered email address.
        /// </summary>
        public static string SentPasswordToEmail = "Your new password has been sent to your registered email address.";

        /// <summary>
        /// Message - Your new password has been sent to your registered mobile no.
        /// </summary>
        public static string SentPasswordToMobile = "Your new password has been sent to your registered mobile no.";

        /// <summary>
        /// Message - Your new password has been sent to your registered email address and mobile no.
        /// </summary>
        public static string SentPasswordToBoth = "Your new password has been sent to your registered email address and mobile no.";

        /// <summary>
        /// Message - Please select reset password method either reset via email or message.
        /// </summary>
        public static string ResetMethod = "Please select reset password method either reset via email or message.";

        /// <summary>
        /// Message - Please enter old password.
        /// </summary>
        public static string oldPassword = "Please enter old password.";

        /// <summary>
        /// Message - Please enter new password.
        /// </summary>
        public static string newPassword = "Please enter new password.";

        /// <summary>
        /// Message - Please Re-enter new password.
        /// </summary>
        public static string ReEnterNewPassword = "Please Re-enter new password.";

        /// <summary>
        /// Message - New password and re-enter password not match.
        /// </summary>
        public static string passwordNoMatch = "New password and re-enter password not match.";

        /// <summary>
        /// Message - Your old password and new password must be different, Please change new password.
        /// </summary>
        public static string differentPassword = "Your old password and new password must be different, Please change new password.";

        /// <summary>
        /// Message - Data Deleted Successfully.
        /// </summary>
        public static string Deleted = "Data Deleted Successfully.";
    }
}
