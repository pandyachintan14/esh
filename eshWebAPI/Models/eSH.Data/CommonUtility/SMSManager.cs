﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace eshWebAPI.Models.eSH.Data.CommonUtility
{
    public static class SMSManager
    {
        public static string SendSMS(string mobileNumber, string message)
        {
            ////Your authentication key
            //string authKey = "YourAuthKey";
            ////Multiple mobiles numbers separated by comma
            //string mobileNumber = "9999999";
            ////Sender ID,While using route4 sender id should be 6 characters long.
            //string senderId = "102234";
            ////Your message to send, Add URL encoding here.
            //string message = HttpUtility.UrlEncode("Test message");

            //Prepare you post parameters
            StringBuilder sbPostData = new StringBuilder();

            sbPostData.AppendFormat("username={0}", "websysapp");
            sbPostData.AppendFormat("&password={0}", "websys_963");
            sbPostData.AppendFormat("&senderid={0}", "WEBSYS");
            sbPostData.AppendFormat("&to={0}", mobileNumber);
            sbPostData.AppendFormat("&text={0}", message);
            sbPostData.AppendFormat("&route={0}", "Informative");
            sbPostData.AppendFormat("&type={0}", "text");

            try
            {
                //Call Send SMS API
                string sendSMSUri = "http://sms.websysinfotech.in/api/api_http.php?" + sbPostData.ToString();
                //Create HTTPWebrequest
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(sendSMSUri);
                //Prepare and Add URL Encoded data
                UTF8Encoding encoding = new UTF8Encoding();
                byte[] data = encoding.GetBytes(sbPostData.ToString());
                //Specify post method
                httpWReq.Method = "GET";
                httpWReq.ContentType = "application/x-www-form-urlencoded";
                //   httpWReq.ContentLength = data.Length;
                //using (Stream stream = httpWReq.GetRequestStream())
                //{
                //    stream.Write(data, 0, data.Length);
                //}
                //Get the response
                HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
                StreamReader reader = new StreamReader(response.GetResponseStream());
                string responseString = reader.ReadToEnd();

                //Close the response
                reader.Close();
                response.Close();

                return responseString;
            }
            catch (SystemException ex)
            {
                return ex.Message.ToString();
            }

        }
    }
}
