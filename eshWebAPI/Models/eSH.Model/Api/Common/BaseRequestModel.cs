﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace eSH.Model.Api.Common
{
    public class BaseRequestModel
    {
        [JsonProperty(PropertyName = "Flag")]
        public string Flag { get; set; }
        [JsonProperty(PropertyName = "RegistrationCode")]
        public string RegistrationCode { get; set; }
        [JsonProperty(PropertyName = "LoginID")]
        public int LoginID { get; set; }
    }
}
