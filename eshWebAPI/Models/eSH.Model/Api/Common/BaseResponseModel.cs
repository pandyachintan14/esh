﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eSH.Model.Api.Common
{
    public class BaseResponseModel
    {
        public int Status { get; set; }
        public string Exception { get; set; }
        public string Message { get; set; }
        public object Payload { get; set; }
        public object ValidationSummary { get; set; }
    }
}
