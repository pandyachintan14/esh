﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eSH.Model.Api.Response
{
   public class ProfileManageResponseModel
    {
        public string Registration_Number { get; set; }
        public int Member_Pk { get; set; }
        public string Member_Photo_URL { get; set; }
        public string Member_FullName { get; set; }
        public string Member_Section_1 { get; set; }
        public string Member_Section_1_Value { get; set; }
        public string Member_Section_2 { get; set; }
        public string Member_Section_2_Value { get; set; }
        public string Member_Section_3 { get; set; }
        public string Member_Section_3_Value { get; set; }
        public string Member_Personal_Full_Name { get; set; }
        public string Member_Personal_Citizenship { get; set; }
        public string Member_Personal_DateOfBirth { get; set; }
        public string Member_Personal_Gender { get; set; }
        public string Member_Personal_Ethinicity { get; set; }
        public string Member_Personal_IDType { get; set; }
        public string Member_Personal_IDNumber { get; set; }
        public string Member_Personal_Contact_Mobile_Number { get; set; }
        public string Member_Personal_Contact_Email { get; set; }
        public string Member_Personal_Contact_Address1 { get; set; }
        public string Member_Personal_Contact_Address2 { get; set; }
        public string Member_Personal_Contact_Address3 { get; set; }
        public string Member_Personal_Contact_Others1 { get; set; }
        public string Member_Personal_Contact_Others2 { get; set; }
        public string Member_Personal_Contact_Others3 { get; set; }
        public string Member_Personal_Membership_Origin { get; set; }
        public string Member_Personal_Membership_Type { get; set; }
        public string Member_Personal_Membership_Business_Entity { get; set; }
        public string Member_Personal_Membership_Distributor_Number { get; set; }
        public string Member_Personal_Membership_JoinDate { get; set; }
        public string Member_Personal_Membership_Expiry { get; set; }
        public string Member_Personal_Membership_Type2 { get; set; }
        public string Member_Sponsor_Distributor_Number { get; set; }
        public string Member_Sponsor_Name { get; set; }
        public string Member_Sponsor_Country { get; set; }
        public string Member_Spouse_FullName { get; set; }
        public string Member_Spouse_CitizenShip { get; set; }
        public string Member_Spouse_DOB { get; set; }
        public string Member_Spouse_ID_Type { get; set; }
        public string Member_Spouse_ID_Number { get; set; }
        public string Member_Spouse_Photo { get; set; }
        public string Member_Spouse_MrgCertificate { get; set; }
        public string Member_Guardian_Name { get; set; }
        public string Member_Guardian_RelationsShip { get; set; }
        public string Member_Guardian_IDNumber { get; set; }
        public string Member_Guardian_ContactNumber { get; set; }
        public string Member_CompanyName { get; set; }
        public string Member_Company_Registration_Number { get; set; }
        public string Member_Company_Corporation_Date { get; set; }
        public string Member_Company_Status { get; set; }
        public string Member_Form_49 { get; set; }
        public string Member_Form_24 { get; set; }
        public string Member_Doctors_Resolution { get; set; }
        public string Member_Bank_ACCNumber { get; set; }
        public string Member_Bank_ACC_Name_1 { get; set; }
        public string Member_Bank_ACC_IDCard_1 { get; set; }
        public string Member_Bank_ACC_Name_2 { get; set; }
        public string Member_Bank_ACC_IDCard_2 { get; set; }
        public string Member_Bank_ACC_Type { get; set; }
        public string Member_Bank_ACC_Name { get; set; }
        public string Member_Bank_ACC_Branch { get; set; }
        public string Member_Bank_Statement_Header { get; set; }
        public string Member_Bank_Status { get; set; }
        public string Member_Language { get; set; }
    }
}
