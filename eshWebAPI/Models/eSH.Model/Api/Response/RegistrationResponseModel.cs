﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eSH.Model.Api.Response
{
  public  class RegistrationResponseModel
    {
        public int? Member_PK { set; get; }
        public string Registration_Number { set; get; }
    }
}
