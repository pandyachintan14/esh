﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eSH.Model.Api.Response
{
    public class Login : BaseModelClass
    {
        public Logindetails payload { get; set; }
    }
    public class Logindetails
    {
        public int UserPK { get; set; }
        public int RoleFK { get; set; }
        public int LoginStatus { get; set; }
        public string LoginMessage { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string SessionUsername { get; set; }
        public string SessionEmailAddress { get; set; }
        public string SessionContactNo { get; set; }
        public string OTP { get; set; }
        public string Member_PK { get; set; }
        public string Registration_Number { get; set; }
        public string Member_Full_Name { get; set; }
        public string Membership_Type { get; set; }
      
    }
}
