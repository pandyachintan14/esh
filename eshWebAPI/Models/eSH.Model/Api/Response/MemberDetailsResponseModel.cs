﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eSH.Model.Api.Response
{
    public class MemberDetailsResponseModel
    {
        public string Parent_Name { get; set; }
        public int Parent_FK { get; set; }
        public List<MemberDetailsSubResponseModel> memberChild { get; set; }
    }
    public class MemberDetailsSubResponseModel
    {
        public string Child_Name { get; set; }
        public int Child_Pk { get; set; }
        public int isApproved { get; set; }
        public int isTC { get; set; }
        public string Child_Rejection_Reason { get; set; }
    }
}
