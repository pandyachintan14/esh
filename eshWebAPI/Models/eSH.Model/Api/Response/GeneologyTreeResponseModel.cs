﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eSH.Model.Api.Response
{
  public class GeneologyTreeResponseModel
    {
        public int id { get; set; }
        public int pid { get; set; }
        public string name { get; set; }
        public string title { get; set; }
        public string email { get; set; }
        public string img { get; set; }
    }
}

