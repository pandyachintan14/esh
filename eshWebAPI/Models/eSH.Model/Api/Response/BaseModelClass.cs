﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eSH.Model.Api.Response
{
   public class BaseModelClass
    {
        public int status { get; set; }
        public string message { get; set; }
        public string Exception { get; set; }
    }
}
