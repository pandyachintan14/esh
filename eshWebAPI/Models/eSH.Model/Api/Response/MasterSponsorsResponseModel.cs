﻿using System;
using System.Collections.Generic;
using System.Text;

namespace eSH.Model.Api.Response
{
   public class MasterSponsorsResponseModel
    {
        public int? Sponser_PK { set; get; }
        public string Sponser_Name { set; get; }
        public string Sponser_RegCode { set; get; }
        public string Sponser_Status { set; get; }

    }
}
