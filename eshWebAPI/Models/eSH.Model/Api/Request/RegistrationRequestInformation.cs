﻿using eSH.Model.Api.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace eSH.Model.Api.Request
{
    public class RegistrationRequestInformation : BaseRequestModel
    {
        [JsonProperty(PropertyName = "Member_PK")]
        public int? Member_PK { set; get; }
        [JsonProperty(PropertyName = "Membership_Origin")]
        public string Membership_Origin { set; get; }
        [JsonProperty(PropertyName = "Country_OF_CitizenShip")]
        public string Country_OF_CitizenShip { set; get; }
        [JsonProperty(PropertyName = "isPR")]
        public int? isPR { set; get; }
        [JsonProperty(PropertyName = "Membership_Type")]
        public string Membership_Type { set; get; }
        [JsonProperty(PropertyName = "Register_as_BusinessEntity")]
        public int? Register_as_BusinessEntity { set; get; }
        [JsonProperty(PropertyName = "Member_Email_Address")]
        public string Member_Email_Address { set; get; }
        [JsonProperty(PropertyName = "Member_Mobile_Number")]
        public string Member_Mobile_Number { set; get; }
        [JsonProperty(PropertyName = "Member_Full_Name")]
        public string Member_Full_Name { set; get; }
        [JsonProperty(PropertyName = "Member_Gender")]
        public string Member_Gender { set; get; }
        [JsonProperty(PropertyName = "Member_Ethinicity")]
        public string Member_Ethinicity { set; get; }
        [JsonProperty(PropertyName = "Member_Date_OF_Birth")]
        public string Member_Date_OF_Birth { set; get; }
        [JsonProperty(PropertyName = "Guardian_Name")]
        public string Guardian_Name { set; get; }
        [JsonProperty(PropertyName = "Guardian_Relationship")]
        public string Guardian_Relationship { set; get; }
        [JsonProperty(PropertyName = "Guardian_Identity_Card")]
        public string Guardian_Identity_Card { set; get; }
        [JsonProperty(PropertyName = "Guardian_Contact_Number")]
        public string Guardian_Contact_Number { set; get; }
        [JsonProperty(PropertyName = "Applicant_Identity_Type")]
        public string Applicant_Identity_Type { set; get; }
        [JsonProperty(PropertyName = "Identity_Card_Number")]
        public string Identity_Card_Number { set; get; }
        [JsonProperty(PropertyName = "Member_Address_1")]
        public string Member_Address_1 { set; get; }
        [JsonProperty(PropertyName = "Member_Address_2")]
        public string Member_Address_2 { set; get; }
        [JsonProperty(PropertyName = "Member_Address_3")]
        public string Member_Address_3 { set; get; }
        [JsonProperty(PropertyName = "Member_Town")]
        public string Member_Town { set; get; }
        [JsonProperty(PropertyName = "Member_State")]
        public string Member_State { set; get; }
        [JsonProperty(PropertyName = "Member_Postal")]
        public string Member_Postal { set; get; }
        [JsonProperty(PropertyName = "Member_RukunTetangga")]
        public string Member_RukunTetangga { set; get; }
        [JsonProperty(PropertyName = "Member_RukunWarga")]
        public string Member_RukunWarga { set; get; }
        [JsonProperty(PropertyName = "Member_Language_Preference")]
        public string Member_Language_Preference { set; get; }
        [JsonProperty(PropertyName = "isSpouse")]
        public int? isSpouse { set; get; }
        [JsonProperty(PropertyName = "Spouse_Full_Name")]
        public string Spouse_Full_Name { set; get; }
        [JsonProperty(PropertyName = "Spouse_Nationality")]
        public string Spouse_Nationality { set; get; }
        [JsonProperty(PropertyName = "Spouse_Date_Of_Birth")]
        public string Spouse_Date_Of_Birth { set; get; }
        [JsonProperty(PropertyName = "Spouse_Ethinicity")]
        public string Spouse_Ethinicity { set; get; }
        [JsonProperty(PropertyName = "Spouse_Applicant_Identity_Type")]
        public string Spouse_Applicant_Identity_Type { set; get; }
        [JsonProperty(PropertyName = "Spouse_Identity_Card_Number")]
        public string Spouse_Identity_Card_Number { set; get; }
        [JsonProperty(PropertyName = "Spouse_Mobile_Number")]
        public string Spouse_Mobile_Number { set; get; }
        [JsonProperty(PropertyName = "isRegisterationDone")]
        public int? isRegisterationDone { set; get; }
        [JsonProperty(PropertyName = "Sponsor_Member_Parent_FK")]
        public int? Sponsor_Member_Parent_FK { set; get; }
        [JsonProperty(PropertyName = "isTCConfirmed")]
        public int? isTCConfirmed { set; get; }
        [JsonProperty(PropertyName = "isDeleted")]
        public int? isDeleted { set; get; }
        [JsonProperty(PropertyName = "isApproved")]
        public int? isApproved { set; get; }
        [JsonProperty(PropertyName = "isActive")]
        public int? isActive { set; get; }
        






    }
}
