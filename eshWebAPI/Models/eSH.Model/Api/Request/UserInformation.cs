﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
 

namespace eSH.Model.Api.Request
{
   public class UserInformation
    {
        public User user { get; set; }
    }
    public class User
    {
        [JsonProperty(PropertyName = "UserPK")]
        public int UserPK { get; set; }
        [JsonProperty(PropertyName = "Flag")]
        public string Flag { get; set; }
        [JsonProperty(PropertyName = "UserName")]
        public string UserName { get; set; }
        [JsonProperty(PropertyName = "Password")]
        public string Password { get; set; }
  
       
    }
}
