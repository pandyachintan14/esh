﻿ 
using eSH.Model.Api.Request;
using eSH.Model.Api.Response;
using eshWebAPI.Models.eSH.Data.CommonUtility;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Net;
using System.Text;

namespace eSH.Service.ApiServices
{
    public class LoginService
    {
        
        /// <summary>
     /// it checks the login information
     /// </summary>
        public Login checkLogin(User user)
        {
            Login login = new Login();
            Logindetails logindetails = new Logindetails();
            try
            {

                SqlParameter[] param = {

                    new SqlParameter("@UserName",user.UserName),
                    new SqlParameter("@Password", user.Password),
                };


                DataSet ds = DataBaseAccessLayer.Select(CommonSP.usp_CheckLogin, param);

                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    logindetails.LoginStatus = Convert.ToInt32(ds.Tables[0].Rows[0]["LoginStatus"]);
                    logindetails.LoginMessage = ds.Tables[0].Rows[0]["LoginStatusMessage"].ToString();
                    if (logindetails.LoginStatus == 1)
                    {
                        if (ds.Tables[1].Rows.Count > 0)
                        {
                            logindetails.SessionUsername = Convert.ToString(ds.Tables[1].Rows[0]["UserName"]);
                            logindetails.SessionEmailAddress = Convert.ToString(ds.Tables[1].Rows[0]["EmailAddress"]);
                            logindetails.SessionContactNo = Convert.ToString(ds.Tables[1].Rows[0]["ContactNumber"]);
                            logindetails.UserPK = Convert.ToInt32(ds.Tables[1].Rows[0]["UserPK"]);
                            logindetails.Member_PK = Convert.ToString(ds.Tables[1].Rows[0]["Member_PK"]);
                            logindetails.Registration_Number = Convert.ToString(ds.Tables[1].Rows[0]["Registration_Number"]);
                            logindetails.Member_Full_Name = Convert.ToString(ds.Tables[1].Rows[0]["Member_Full_Name"]);
                            logindetails.Membership_Type = Convert.ToString(ds.Tables[1].Rows[0]["Membership_Type"]);
                        }
                    }
                    login.payload = logindetails;
                    login.status = 1;
                    login.message = "Success";
                    login.Exception = "";
                }
            }
            catch (Exception ex)
            {
                login.status = -1;
                login.message = "Something went Wrong.";
                login.Exception = ex.InnerException.Message.ToString();
            }
            return login;
        }
 
    }
}
