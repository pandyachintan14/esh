$(function () {



    $("#form-register").validate({
        rules: {
            password: {
                required: true,
            },
            confirm_password: {
                equalTo: "#password"
            }
        },
        messages: {
            username: {
                required: "Please provide an username"
            },
            email: {
                required: "Please provide an email"
            },
            password: {
                required: "Please provide a password"
            },
            confirm_password: {
                required: "Please provide a password",
                equalTo: "Please enter the same password"
            }
        }
    });
    $("#form-total").steps({
        headerTag: "h2",
        bodyTag: "section",
        transitionEffect: "none",
        // enableAllSteps: true,
        autoFocus: false,
        transitionEffectSpeed: 0,
        titleTemplate: '<div class="title">#title#</div>',
        labels: {
            previous: 'Back',
            next: 'Continue',
            finish: 'PAY NOW',
            current: ''
        },
        onStepChanging: function (event, currentIndex, newIndex) {
            
             
            return validations();
        }

    });
});
function saveAllinformation() {


    var user = JSON.stringify({
        "Member_PK": "-1",
        "Membership_Origin": $("#ddlMembershipOrigin").val(),
        "Country_OF_CitizenShip": $("#ddlCountryOfCitizenship").val(),
        "isPR": ($("#chkForPR").is(":checked") ? 1 : 0),
        "Membership_Type": $("#ddlMembershipType").val(),
        "Register_as_BusinessEntity": ($("#chkBusinessEntity").is(":checked") ? 1 : 0),
        "Member_Email_Address": $("#txt_member_email").val(),
        "Member_Mobile_Number": $("#txt_member_mobile_Number").val() + "-" + $("#txt_member_mobile_Number_2").val(),
        "Member_Full_Name": $("#txt_Full_Name").val(),
        "Member_Gender": ($("#rdGenderM").is(":checked") ? "MALE" : "FEMALE"),
        "Member_Ethinicity": ($("#rdMalay").is(":checked") ? "MALAY" : ($("#rdChiness").is(":checked") ? "CHINESE" : ($("#rdIndian").is(":checked") ? "INDIAN" : "OTHERS"))),
        "Member_Date_OF_Birth": $("#txt_Member_Date_OF_Birth").val(),
        "Guardian_Name": $("#txt_guardian_name").val(),
        "Guardian_Relationship": $("#ddlApplicantRelationship").val(),
        "Guardian_Identity_Card": $("#txt_Guardian_Identity_Card").val(),
        "Guardian_Contact_Number": $("#text_guardian_contact_number").val(),
        "Applicant_Identity_Type": $("#ddlApplicantIdentityType").val(),
        "Identity_Card_Number": $("#txt_Identity_Card_Number").val(),
        "Member_Address_1": $("#txt_Address1").val(),
        "Member_Address_2": $("#txt_Address2").val(),
        "Member_Address_3": $("#txt_Address3").val(),
        "Member_Town": $("#txt_Town").val(),
        "Member_State": $("#txt_State").val(),
        "Member_Postal": $("#txt_Postal").val(),
        "Member_RukunTetangga": $("#txt_Rukun").val(),
        "Member_RukunWarga": $("#txt_RW").val(),
        "Member_Language_Preference": ($("#rdBhasa").is(":checked") ? "BAHASA MALAYSIA" : ($("#rdEnglish").is(":checked") ? "ENGLISH" : ($("#rdChiness").is(":checked") ? "CHINESE" : "OTHERS"))),
        "isSpouse": ($("#chkAddSpouse").is(":checked") ? "1" : "0"),
        "Spouse_Full_Name": $("#txt_Spouse_Full_Name").val(),
        "Spouse_Nationality": $("#ddlNationality").val(),
        "Spouse_Date_Of_Birth": $("#txt_SpouseDateOfBirth").val(),
        "Spouse_Ethinicity": ($("#rdSpMalay").is(":checked") ? "MALAY" : ($("#rdSpChiness").is(":checked") ? "CHINESE" : ($("#rdSpIndian").is(":checked") ? "INDIAN" : "OTHERS"))),
        "Spouse_Applicant_Identity_Type": $("#ddlSpouseIdentityType").val(),
        "Spouse_Identity_Card_Number": $("#txt_Identity_Card_Number").val(),
        "Spouse_Mobile_Number": $("#txt_spouse_Mobile_Number").val() + "-" + $("#txt_spouse_Mobile_Number2").val(),
        "isRegisterationDone": "0",
        "Sponsor_Member_Parent_FK": $("#ddlSponsor").val(),
        "isTCConfirmed": "0",
        "isDeleted": "0",
        "isApproved": "1",
        "isActive": "0",
        "Flag": "Add",
        "LoginID": UserPK
    });

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "http://localhost:58494/api/MemberRegistration/ManageMemberRegistrations",
        data: user,
        async: false,
        dataType: "json",
        success: OnSuccess,
        error: function (XMLHttpRequest) {

            if (XMLHttpRequest.status == 401) {
                // alert(SessionClientID);
                window.location.href = '/Login/Signin';
            }
        },
    });
    function OnSuccess(data) {
        console.log(data);
        debugger;
        if (data != -1) {

            if (data.payload != null) {
                //$("#ddlSponsor").append($("<option/>").val('0').text('Select Sponsor'));
                //for (var i = 0; i < data.payload.length; i++) {
                //    $("#ddlSponsor").append($("<option/>").val(data.payload[i].sponser_pk).text(data.payload[i].sponser_name + " " + data.payload[i].sponser_reg_code + " " + data.payload[i].sponser_status));
                //}
            }
        }

    }

}
function getAge(dateString) {
    var today = new Date();
    var birthDate = new Date(dateString);
    var age = today.getFullYear() - birthDate.getFullYear();
    var m = today.getMonth() - birthDate.getMonth();
    if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
        age--;
    }
    return age;
}
function validations() {
    debugger;
    var requiredFieldValidations = "";

    var ddlMembershipOrigin = $("#ddlMembershipOrigin").val();
    var ddlCountryOfCitizenship = $("#ddlCountryOfCitizenship").val();
    var ddlMembershipType = $("#ddlMembershipType").val();
    var txt_Full_Name = $("#txt_Full_Name").val();


    var txt_guardian_name = $("#txt_guardian_name").val();
    var ddlApplicantRelationship = $("#ddlApplicantRelationship").val();
    var txt_Guardian_Identity_Card = $("#txt_Guardian_Identity_Card").val();
    var text_guardian_contact_number = $("#text_guardian_contact_number").val();

    var ddlApplicantIdentityType = $("#ddlApplicantIdentityType").val();
    var txt_Identity_Card_Number = $("#txt_Identity_Card_Number").val();
    var txt_Address1 = $("#txt_Address1").val();
    var txt_Address2 = $("#txt_Address2").val();
    var txt_Address3 = $("#txt_Address3").val();
    var txt_Town = $("#txt_Town").val();

    var txt_State = $("#txt_State").val();
    var txt_Postal = $("#txt_Postal").val();

    var rdBhasa = $("#rdBhasa").is(":checked");
    var rdEnglish = $("#rdEnglish").is(":checked");
    var rdChiness = $("#rdChiness").is(":checked");

    var txt_Spouse_Full_Name = $("#txt_Spouse_Full_Name").val();
    var ddlNationality = $("#ddlNationality").val();


    var rdSpMalay = $("#rdSpMalay").is(":checked");
    var rdSpChiness = $("#rdSpChiness").is(":checked");
    var rdSpIndian = $("#rdSpIndian").is(":checked");
    var rdSpOthers = $("#rdSpOthers").is(":checked");


    var ddlSpouseIdentityType = $("#ddlSpouseIdentityType").val();

    var txt_Identity_Card_Number = $("#txt_Identity_Card_Number").val();

    var rdGenderM = $("#rdGenderM").is(":checked");
    var rdGenderF = $("#rdGenderF").is(":checked");
    var rdMalay = $("#rdMalay").is(":checked");
    var rdChiness = $("#rdChiness").is(":checked");
    var rdIndian = $("#rdIndian").is(":checked");
    var rdOthers = $("#rdOthers").is(":checked");

    var txt_Full_Name = $("#txt_Full_Name").val();


    if (ddlMembershipOrigin == "SELECT") {
        requiredFieldValidations = requiredFieldValidations + "Membership Origin is required. \n";
        $("#ddlMembershipOrigin").focus();
    }
    if (ddlCountryOfCitizenship == "SELECT") {
        requiredFieldValidations = requiredFieldValidations + "Country Of Citizenship is required. \n";
        $("#ddlCountryOfCitizenship").focus();
    }
    if (ddlMembershipType == "SELECT") {
        requiredFieldValidations = requiredFieldValidations + "Membership Type is required. \n"; $("#ddlMembershipType").focus();
    }
    if (txt_Full_Name == "") {
        requiredFieldValidations = requiredFieldValidations + "Full Name is required. \n"; $("#txt_Full_Name").focus();
    }
    if (rdGenderM == false && rdGenderF == false) {
        requiredFieldValidations = requiredFieldValidations + "Gender is required. \n"; $("#rdGenderM").focus();
    }
    //if (rdMalay == false && rdChiness == false && rdIndian == false && rdOthers == false) {
    //    requiredFieldValidations = requiredFieldValidations + "Ethinicity is required. \n"; $("#rdMalay").focus();
    //    }
    if ($("#txt_Member_Date_OF_Birth").val() != "") {
        debugger;
        var ddlMembershipOrigin = $("#ddlMembershipOrigin").val();
        var age = getAge($("#txt_Member_Date_OF_Birth").val());

        if (age < 18) {
            requiredFieldValidations = requiredFieldValidations + "Age must be above 18 years. \n";

        }
        else if (age >= 18 && age <= 20 && ddlMembershipOrigin == "SINGAPORE") {

            if (txt_guardian_name == "") {
                requiredFieldValidations = requiredFieldValidations + "Guardian name is required. \n";
                $("#txt_guardian_name").focus();
            }
            if (ddlApplicantRelationship == "SELECT") {
                requiredFieldValidations = requiredFieldValidations + "Guardian Relationship is required. \n";
                $("#ddlApplicantRelationship").focus();
            }
            if (txt_Guardian_Identity_Card == "") {
                requiredFieldValidations = requiredFieldValidations + "Guardian Identity Card is required. \n";
                $("#txt_Guardian_Identity_Card").focus();
            }
            if (text_guardian_contact_number == "") {
                requiredFieldValidations = requiredFieldValidations + "Guardian Contact Number is required. \n";
                $("#text_guardian_contact_number").focus();
            }


        }
        else if (age == 18) {
            if (txt_guardian_name == "") {
                requiredFieldValidations = requiredFieldValidations + "Guardian name is required. \n";
                $("#txt_guardian_name").focus();
            }
            if (ddlApplicantRelationship == "SELECT") {
                requiredFieldValidations = requiredFieldValidations + "Guardian Relationship is required. \n";
                $("#ddlApplicantRelationship").focus();
            }
            if (txt_Guardian_Identity_Card == "") {
                requiredFieldValidations = requiredFieldValidations + "Guardian Identity Card is required. \n";
                $("#txt_Guardian_Identity_Card").focus();
            }
            if (text_guardian_contact_number == "") {
                requiredFieldValidations = requiredFieldValidations + "Guardian Contact Number is required. \n";
                $("#text_guardian_contact_number").focus();
            }
        }
    }

    if (ddlApplicantIdentityType == "SELECT") {
        requiredFieldValidations = requiredFieldValidations + "Applicant Identity Type is required. \n";
        $("#ddlApplicantIdentityType").focus();
    }
    if (txt_Identity_Card_Number == "") {
        requiredFieldValidations = requiredFieldValidations + "Identity Number is required. \n";
        $("#txt_Identity_Card_Number").focus();
    }
    if (txt_Address1 == "") {
        requiredFieldValidations = requiredFieldValidations + "Address 1 is required. \n";
        $("#txt_Address1").focus();
    }
    if (txt_Address2 == "") {
        requiredFieldValidations = requiredFieldValidations + "Address 2 is required. \n";
        $("#txt_Address2").focus();
    }
    if (txt_Address3 == "") {
        requiredFieldValidations = requiredFieldValidations + "Address 3 is required. \n";
        $("#txt_Address3").focus();
    }
    if (txt_Town == "") {
        requiredFieldValidations = requiredFieldValidations + "Town is required. \n";
        $("#txt_Town").focus();
    }
    if (txt_State == "") { requiredFieldValidations = requiredFieldValidations + "State is required. \n"; $("#txt_State").focus(); }
    if (txt_Postal == "") {
        requiredFieldValidations = requiredFieldValidations + "Postal Code is required. \n";
        $("#txt_Postal").focus();
    }

    if (rdBhasa == false && rdEnglish == false && rdChiness == false) {
        requiredFieldValidations = requiredFieldValidations + "Language Preferences are required. \n";
        $("#rdBhasa").focus();
    }

    if ($("#chkAddSpouse").is(":checked")) {

        var ddlSpouseIdentityType = $("#ddlSpouseIdentityType").val();

        var txt_Identity_Card_Number = $("#txt_Identity_Card_Number").val();

        if (ddlSpouseIdentityType == "SELECT") {
            requiredFieldValidations = requiredFieldValidations + "Spouse Identity Type is required. \n";
            $("#ddlSpouseIdentityType").focus();
        }
        if (txt_Identity_Card_Number == "") {
            requiredFieldValidations = requiredFieldValidations + "Spouse Identity Number is required. \n";
            $("#txt_Identity_Card_Number").focus();
        }
        if (txt_Spouse_Full_Name == "") {
            requiredFieldValidations = requiredFieldValidations + "Spouse Full Name is required. \n";
            $("#txt_Spouse_Full_Name").focus();
        }
        if (ddlNationality == "") {
            requiredFieldValidations = requiredFieldValidations + "Spouse Nationality is required. \n";
            $("#ddlNationality").focus();
        }
        if (rdSpMalay == false && rdSpChiness == false && rdSpIndian == false && rdSpOthers == false) {
            requiredFieldValidations = requiredFieldValidations + "Spouse Ethinicity is required. \n";
            $("#rdSpMalay").focus();
        }
    }


    if (requiredFieldValidations == "")
        return true;
    else {
        alert(requiredFieldValidations);
        return false;
    }
}