﻿$(function () {
    bindprofile();
});
function bindprofile() {
    var user = JSON.stringify({ "Flag": "NA", "RegistrationCode": -1, "LoginID": Member_PK });
    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/api/Profile/GetMemberProfileDetails",
        data: user,
        async: false,
        dataType: "json",
        success: OnSuccess,
        error: function (XMLHttpRequest) {

            if (XMLHttpRequest.status == 401) {
                // alert(SessionClientID);
                window.location.href = '/Login/Signin';
            }
        },
    });
    function OnSuccess(data) {
        console.log(data);
        debugger;
        if (data != -1) {

            if (data.payload != null) {
                
                for (var i = 0; i < data.payload.length; i++) {
                    $("#hFullName").text(data.payload[i].member_FullName);

                    $("#pRegistrationCode").text(data.payload[i].registration_Number);
                    $("#sp1").text(data.payload[i].member_Section_1_Value);
                    $("#sp2").text(data.payload[i].member_Section_2_Value);
                    $("#sp3").text(data.payload[i].member_Section_3_Value);
                    $("#spnFullName").text(data.payload[i].member_Personal_Full_Name);
                    $("#spncitizenship").text(data.payload[i].member_Personal_Citizenship);
                    $("#spnDOB").text(data.payload[i].member_Personal_DateOfBirth);
                    $("#spnGender").text(data.payload[i].member_Personal_Gender);
                    $("#spnEthinicity").text(data.payload[i].member_Personal_Ethinicity);
                    $("#spnIdType").text(data.payload[i].member_Personal_IDType);
                    $("#spnIDNumber").text(data.payload[i].member_Personal_IDNumber);

                    $("#spnMobileNumber").text(data.payload[i].member_Personal_Contact_Mobile_Number);
                    $("#spnEmail").text(data.payload[i].member_Personal_Contact_Email);
                    $("#spnAddress").text(data.payload[i].member_Personal_Contact_Address1 + '\n' + data.payload[i].member_Personal_Contact_Address2 + '\n' + data.payload[i].member_Personal_Contact_Address3 + '\n' + data.payload[i].member_Personal_Contact_Others1 + '\n' + data.payload[i].member_Personal_Contact_Others2 + '\n' + data.payload[i].member_Personal_Contact_Others3);

                    $("#spnMembershipOrigin").text(data.payload[i].member_Personal_Membership_Origin);
                    $("#spnMemberType").text(data.payload[i].member_Personal_Membership_Type);
                    $("#spnBusinessEntity").text(data.payload[i].member_Personal_Membership_Business_Entity);
                    $("#spnDistNumber").text(data.payload[i].member_Personal_Membership_Distributor_Number);
                    $("#spnJoinDate").text(data.payload[i].member_Personal_Membership_JoinDate);
                    $("#spnMembershipExpiry").text(data.payload[i].member_Personal_Membership_Expiry);
                    $("#spnMembershipType").text(data.payload[i].member_Personal_Membership_Type2);
                    
                    $("#spnDistributorNumber").text(data.payload[i].member_Sponsor_Distributor_Number);
                    $("#spnSponsorName").text(data.payload[i].member_Sponsor_Name);
                    $("#spnCountry").text(data.payload[i].member_Sponsor_Country);
                    $("#spnSpouseFullName").text(data.payload[i].member_Spouse_FullName);
                    $("#spnSpouseCitizenship").text(data.payload[i].member_Spouse_CitizenShip);
                    $("#spnSpouseDOB").text(data.payload[i].member_Spouse_DOB);

                    $("#spnSpouseIDType").text(data.payload[i].member_Spouse_Id_Type);
                    $("#spnSpouseIDNumber").text(data.payload[i].member_Spouse_Id_Number);
                    $("#spnGuardianFullName").text(data.payload[i].member_Guardian_Name);

                    $("#spnSpouseRelationship").text(data.payload[i].member_Guardian_RelationsShip);
                    $("#spnIDPassport").text(data.payload[i].member_Guardian_IDNumber);
                    $("#spnGContactNumber").text(data.payload[i].member_Guardian_ContactNumber);
                    $("#spnCompanyName").text(data.payload[i].member_Company_Name);
                    $("#spnRegistrationNumber").text(data.payload[i].member_Company_Registration_Number);
                    $("#spnInCoporationDate").text(data.payload[i].member_Company_Corporation_Date);

                    $("#spnForm49").text(data.payload[i].member_Form_49);
                    $("#spnForm24").text(data.payload[i].member_Form_24);
                    $("#spnDocRe").text(data.payload[i].member_Doctors_Resolution);


                    $("#spnAccNumber").text(data.payload[i].member_Bank_ACCNumber);
                    $("#spnAccName1").text(data.payload[i].member_Bank_ACC_Name_1);
                    $("#spnIDCard1").text(data.payload[i].member_Bank_ACC_IDCard_1);
                    $("#spnAccName2").text(data.payload[i].member_Bank_ACC_Name_2);
                    $("#spnIDCard2").text(data.payload[i].member_Bank_ACC_IDCard_2);
                    $("#spnAccountType").text(data.payload[i].member_Bank_ACC_Type);
                    $("#spnBankName").text(data.payload[i].member_Bank_ACC_Name);
                    $("#spnBranch").text(data.payload[i].member_Bank_ACC_Branch);
                    $("#spnLanguage").text(data.payload[i].member_Language);

                  
                     
                }
                
            }
        }

    }
}